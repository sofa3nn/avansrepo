﻿using Domain;
using Domain.Persons;
using Domain.Sprints.ReleaseSprints;
using Domain.Sprints.ReviewSprints;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace UnitTests
{
    public class BacklogTests
    {
        [Fact]
        public void TestIfBacklogCanHaveMoreThanOneItem() 
        {
            //Arrange
            Backlog backlog = new Backlog();
            Item item = new Item(0,"");
            Item item1 = new Item(1,"");
            //Act
            backlog.addItem(item);
            backlog.addItem(item1);
            //Assert
            Assert.Equal(item,backlog.ItemList[0]);
            Assert.Equal(item1, backlog.ItemList[1]);
        }
        [Fact]
        public void TestIfItemCanBeAddedToAReleaseSprintFromBacklog()
        {
            //Arrange
            Mock<ScrumMaster> scrumMaster = new Mock<ScrumMaster>(1, "", "", "");
            Mock<ProductOwner> productOwner = new Mock<ProductOwner>(1, "", "", "");
            Mock<List<Developer>> developers = new Mock<List<Developer>>();
            Project project = new Project("","", developers.Object, scrumMaster.Object, productOwner.Object);
            Backlog backlog = new Backlog();
            Sprint sprint = new ReleaseSprint(1,"",new DateTime(),new DateTime());
            Item item = new Item(0, "");
            Item item1 = new Item(1, "");
            //Act
            project.Backlog = backlog;
            project.AddSprint(sprint);
            project.Backlog.addItem(item);
            project.Backlog.addItem(item1);
            project.AddItemFromBacklogToSprint(item,sprint);
            //Assert
            Assert.Equal(item, project.Sprints[0].ItemList[0]);
            Assert.Equal(item1, project.Backlog.ItemList[0]);
        }
        [Fact]
        public void TestIfItemCanBeAddedToAReviewSprintFromBacklog()
        {
            //Arrange
            Mock<ScrumMaster> scrumMaster = new Mock<ScrumMaster>(1, "", "", "");
            Mock<ProductOwner> productOwner = new Mock<ProductOwner>(1, "", "", "");
            Mock<List<Developer>> developers = new Mock<List<Developer>>();
            Project project = new Project("", "", developers.Object, scrumMaster.Object, productOwner.Object);
            Backlog backlog = new Backlog();
            Sprint sprint = new ReviewSprint(1, "", new DateTime(), new DateTime());
            Item item = new Item(0, "");
            Item item1 = new Item(1, "");
            //Act
            project.Backlog = backlog;
            project.AddSprint(sprint);
            project.Backlog.addItem(item);
            project.Backlog.addItem(item1);
            project.AddItemFromBacklogToSprint(item, sprint);
            //Assert
            Assert.Equal(item, project.Sprints[0].ItemList[0]);
            Assert.Equal(item1, project.Backlog.ItemList[0]);
        }
    }
}
