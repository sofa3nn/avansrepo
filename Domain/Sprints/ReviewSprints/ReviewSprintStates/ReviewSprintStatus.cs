﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Sprints.ReviewSprints.ReviewSprintStates
{
    public abstract class ReviewSprintStatus
    {
        public ReviewSprint ReviewSprint { get; set; }

        public ReviewSprintStatus(ReviewSprint reviewSprint)
        {
            ReviewSprint = reviewSprint;
        }

        public abstract void Start();
        public abstract void Finish();
        public abstract void Review();
        public abstract void Complete();
    }
}
