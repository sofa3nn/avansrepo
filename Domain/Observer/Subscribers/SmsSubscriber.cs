﻿using Domain.ItemStates;
using Domain.Sprints.ReleaseSprints.ReleaseSprintStates;
using Domain.Sprints.ReviewSprints.ReviewSprintStates;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Observer.Subscribers
{
    public class SmsSubscriber : Subscriber
    {
        public string TelephoneNumber;
        public SmsSubscriber(string telephoneNumber)
        {
            TelephoneNumber = telephoneNumber;
        }
        public void updateItem(ItemStatus Status, bool NewMessage)
        {
            if (NewMessage)
            {
                Console.WriteLine("New message added to Item");
                Console.WriteLine("SMS send");
            }
            else
            {
                Console.WriteLine("Item:" + Status.ToString());
                Console.WriteLine("SMS send");
            }
        }

        public void updatePipeline(string Name)
        {
            Console.WriteLine("Name Pipeline:"+ Name);
            Console.WriteLine("SMS send");
        }

        public void updateReleaseSprint(ReleaseSprintStatus Status)
        {
            Console.WriteLine("Status:" + Status.ToString());
            Console.WriteLine("SMS send");
        }
        public void updateReviewSprint(ReviewSprintStatus Status)
        {
            Console.WriteLine("Status:" + Status.ToString());
            Console.WriteLine("SMS send");
        }
    }
}
