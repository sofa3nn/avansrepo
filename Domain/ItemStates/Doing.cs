﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.ItemStates
{
    public class Doing : ItemStatus
    {
        public Doing(Item item) : base(item)
        {
        }

        public override void ChooseItem()
        {
            Console.WriteLine("This Item has already been chosen.");
        }
        
        public override void Finish()
        {
            Item.setStatus(new ReadyForTesting(Item));
            Item.publish();
            Console.WriteLine("This Item is ReadyForTesting.");
        }

        public override void Test()
        {
            Console.WriteLine("This Item has to be ReadyForTesting first.");
        }

        public override void ReDo()
        {
            Console.WriteLine("An item has to be tested first.");
        }

        public override void FinishedTesting()
        {
            Console.WriteLine("This Item has to be tested first.");
        }
        
        public override void Complete()
        {
            Console.WriteLine("This Item has to be finished testing first.");
        }
    }
}
