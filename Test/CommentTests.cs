﻿using Domain;
using Domain.Comments;
using Domain.Comments.Visitors;
using Domain.ItemStates;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace UnitTests
{
    public class CommentTests
    {
        [Fact]
        public void TestIfCommentCanBeAddedToAnItem() 
        {
            StringWriter sw = new StringWriter();
            Console.SetOut(sw);
            //Arrange
            Item item = new Item(0, "Test");
            ConcreteVisitor concretVisitor = new ConcreteVisitor();
            Comment comment = new Comment("Test");
            //Act
            item.AddComment(comment, null);
            StringBuilder sb = sw.GetStringBuilder();
            sb.Remove(0, sb.Length);
            item.MessageGroup.AcceptVisitor(concretVisitor, 0);
            //Assert
            Assert.Equal("MessageGroup:\n--Test\n", sw.ToString());
            StreamWriter standardOutput = new StreamWriter(Console.OpenStandardOutput());
            standardOutput.AutoFlush = true;
            Console.SetOut(standardOutput);
        }
        [Fact]
        public void TestIfCommentCantBeAddedToAnItemWhenDone()
        {
            StringWriter sw = new StringWriter();
            Console.SetOut(sw);
            //Arrange
            Item item = new Item(0, "Test");
            item.setStatus(new Done(item));
            ConcreteVisitor concretVisitor = new ConcreteVisitor();
            Comment comment = new Comment("Test");
            //Act
            item.AddComment(comment, null);
            StringBuilder sb = sw.GetStringBuilder();
            sb.Remove(0, sb.Length);
            item.MessageGroup.AcceptVisitor(concretVisitor, 0);
            //Assert
            Assert.Equal("MessageGroup:\n", sw.ToString());
            StreamWriter standardOutput = new StreamWriter(Console.OpenStandardOutput());
            standardOutput.AutoFlush = true;
            Console.SetOut(standardOutput);
        }
        [Fact]
        public void TestIfCommentCanBePlacedUnderAnOtherCommentToAnItem()
        {
            StringWriter sw = new StringWriter();
            Console.SetOut(sw);
            //Arrange
            Item item = new Item(0, "Test");
            ConcreteVisitor concretVisitor = new ConcreteVisitor();
            Comment comment = new Comment("Test");
            Comment comment1 = new Comment("Test2");
            //Act
            item.AddComment(comment, null);
            item.AddComment(comment1, comment);
            
            StringBuilder sb = sw.GetStringBuilder();
            sb.Remove(0, sb.Length);
            item.MessageGroup.AcceptVisitor(concretVisitor, 0);
            //Assert
            Assert.Equal("MessageGroup:\n--Test\n----Test2\n", sw.ToString());
            StreamWriter standardOutput = new StreamWriter(Console.OpenStandardOutput());
            standardOutput.AutoFlush = true;
            Console.SetOut(standardOutput);
        }
        [Fact]
        public void TestIfCommentCanBeChangedInAnItem()
        {
            StringWriter sw = new StringWriter();
            Console.SetOut(sw);
            //Arrange
            Item item = new Item(0, "Test");
            ConcreteVisitor concretVisitor = new ConcreteVisitor();
            Comment comment = new Comment("Test");
            //Act
            item.AddComment(comment, null);
            item.ChangeComment("Test2", comment);
            StringBuilder sb = sw.GetStringBuilder();
            sb.Remove(0, sb.Length);
            item.MessageGroup.AcceptVisitor(concretVisitor, 0);
            //Assert
            Assert.Equal("MessageGroup:\n--Test2\n", sw.ToString());
            StreamWriter standardOutput = new StreamWriter(Console.OpenStandardOutput());
            standardOutput.AutoFlush = true;
            Console.SetOut(standardOutput);
        }
        [Fact]
        public void TestIfCommentCantBeChangedInAnItemWhenDone()
        {
            StringWriter sw = new StringWriter();
            Console.SetOut(sw);
            //Arrange
            Item item = new Item(0, "Test");
            
            ConcreteVisitor concretVisitor = new ConcreteVisitor();
            Comment comment = new Comment("Test");
            //Act
            item.AddComment(comment, null);
            item.setStatus(new Done(item));
            item.ChangeComment("Test2", comment);
            StringBuilder sb = sw.GetStringBuilder();
            sb.Remove(0, sb.Length);
            item.MessageGroup.AcceptVisitor(concretVisitor, 0);
            //Assert
            Assert.Equal("MessageGroup:\n--Test\n", sw.ToString());
            StreamWriter standardOutput = new StreamWriter(Console.OpenStandardOutput());
            standardOutput.AutoFlush = true;
            Console.SetOut(standardOutput);
        }
    }
}
