﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.ItemStates
{
    public class ReadyForTesting : ItemStatus
    {
        public ReadyForTesting(Item item) : base(item)
        {
        }

        public override void ChooseItem()
        {
            Console.WriteLine("This Item has already been chosen.");
        }

        public override void Finish()
        {
            Console.WriteLine("This Item has already been finished.");
        }

        public override void Test()
        {
            Item.setStatus(new Testing(Item));
            Console.WriteLine("This Item is being tested.");
        }

        public override void ReDo()
        {
            Console.WriteLine("An item has to be tested first.");
        }

        public override void FinishedTesting()
        {
            Console.WriteLine("Item has to be tested first.");
        }

        public override void Complete()
        {
            Console.WriteLine("This Item has not been finished testing yet.");
        }
    }
}
