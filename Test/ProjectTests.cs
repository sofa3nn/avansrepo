﻿using Domain;
using Domain.Persons;
using Domain.Sprints.ReleaseSprints;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace UnitTests
{
    public class ProjectTests
    {
        [Fact]
        public void TestIfProjectIsCreatedWithABacklog() 
        {
            //Arrange
            Mock<ScrumMaster> scrumMaster = new Mock<ScrumMaster>(1, "", "", "");
            Mock<ProductOwner> productOwner = new Mock<ProductOwner>(1, "", "", "");
            Project project = new Project("", "", new List<Developer>(), scrumMaster.Object, productOwner.Object);

            //Act

            //Assert

            Assert.NotNull(project.Backlog);

        }
        [Fact]
        public void TestIfSprintsCanBeAddedToAProject()
        {
            //Arrange
            Mock<ScrumMaster> scrumMaster = new Mock<ScrumMaster>(1,"","","");
            Mock<ProductOwner> productOwner = new Mock<ProductOwner>(1,"","","");
            Project project = new Project("", "", new List<Developer>(), scrumMaster.Object, productOwner.Object);
            Sprint sprint = new ReleaseSprint(1, "", new DateTime(),new DateTime());
            Sprint sprint2 = new ReleaseSprint(1, "", new DateTime(),new DateTime());

            //Act
            project.AddSprint(sprint);
            project.AddSprint(sprint2); 
            //Assert

            Assert.NotNull(project.Sprints);
            Assert.True(project.Sprints[0] == sprint);
            Assert.True(project.Sprints[1] == sprint2);

        }
        [Fact]
        public void TestIfProjectCanHaveNoSprints()
        {
            //Arrange
            Mock<ScrumMaster> scrumMaster = new Mock<ScrumMaster>(1, "", "", "");
            Mock<ProductOwner> productOwner = new Mock<ProductOwner>(1, "", "", "");
            Project project = new Project("", "", new List<Developer>(), scrumMaster.Object, productOwner.Object);

            //Act

            //Assert

            Assert.Equal(new List<Sprint>(), project.Sprints);


        }
        [Fact]
        public void TestIfPersonsCanBeAddedToAProject()
        {
            //Arrange
            Project project = new Project("", "", new List<Developer>(), new ScrumMaster(1, "", "", ""), new ProductOwner(1, "", "", ""));
            Developer developer = new Developer(1, "","","");
            //Act
            project.AddDeveloper(developer);
            //Assert

            Assert.NotNull(project.Developers);
            Assert.True(project.Developers[0] == developer);
            Assert.NotNull(project.ScrumMaster);
            Assert.NotNull(project.ProductOwner);

        }
    }
}
