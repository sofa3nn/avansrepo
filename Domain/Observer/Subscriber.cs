﻿using Domain.ItemStates;
using Domain.Sprints.ReleaseSprints.ReleaseSprintStates;
using Domain.Sprints.ReviewSprints.ReviewSprintStates;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Observer
{
    public interface Subscriber
    {
        void updateItem(ItemStatus Status, bool NewMessage);
        void updatePipeline(string Name);
        void updateReleaseSprint(ReleaseSprintStatus Status);
        void updateReviewSprint(ReviewSprintStatus Status);
    }
}
