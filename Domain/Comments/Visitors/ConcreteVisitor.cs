﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Comments.Visitors
{
    public class ConcreteVisitor : Visitor
    {
        public override void VisitComment(Comment comment, int depth)
        {
            Console.WriteLine(new String('-', depth) + comment.Text);
            
            
            
        }

        public override void VisitMessageGroup(MessageGroup messageGroup)
        {
            Console.WriteLine("MessageGroup:");
        }
    }
}
