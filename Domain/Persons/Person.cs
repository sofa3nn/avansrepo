﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Persons
{
    public abstract class Person
    {
        public int Id { get; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string TelephoneNumber { get; set; }

        public Person(int id, string name, string email, string telephoneNumber)
        {
            Id = id;
            Name = name;
            Email = email;
            TelephoneNumber = telephoneNumber;
        }

    }
}
