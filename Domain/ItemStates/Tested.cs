﻿using Domain.ActivityStates;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.ItemStates
{
    public class Tested : ItemStatus
    {
        public Tested(Item item) : base(item)
        {
        }

        public override void ChooseItem()
        {
            Console.WriteLine("This Item has already been chosen.");
        }

        public override void Finish()
        {
            Item.setStatus(new ReadyForTesting(Item));
            Console.WriteLine("This Item is returned to ready for testing, because of disapproval.");
        }

        public override void Test()
        {
            Console.WriteLine("This Item is already in testing.");
        }

        public override void ReDo()
        {
            Console.WriteLine("This Item has been tested and approved.");
        }

        public override void FinishedTesting()
        {
            Console.WriteLine("This Item is already finished testing.");
        }

        public override void Complete()
        {
            if (Item.Activities == new List<Activity>())
            {
                Item.setStatus(new Done(Item));
                Console.WriteLine("This Item is completed.");
            }
            else 
            {
                bool AllDone = true;
                foreach (Activity activity in Item.Activities) 
                {
                    if (activity.Status.GetType() != new ActivityStates.Done(activity).GetType()) 
                    { 
                        AllDone = false;
                    }
                }
                if (AllDone == true)
                {
                    Item.setStatus(new Done(Item));
                    Console.WriteLine("This Item is completed.");
                }
                else 
                {
                    Console.WriteLine("Not all activities are done.");
                }
            }
            
            
        }
    }
}
