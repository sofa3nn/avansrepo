﻿using Domain.Comments.Visitors;
using Domain.Composites;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Comments
{
    public class Comment : CompositeComponent
    {
        public string Text { get; set; }

        public Comment(string text)
        {
            Text = text;
        }
        public void ChangeText(string text) 
        {
            Text = text;
        }
        public override void AcceptVisitor(Object obj, int depth)
        {
            Visitor? visitor = obj as Visitor;
            visitor?.VisitComment(this, depth);
            base.AcceptVisitor(visitor, depth);
        }
    }
}
