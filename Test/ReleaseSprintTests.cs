﻿using Domain;
using Domain.Pipeline;
using Domain.Pipeline.Visitors;
using Domain.Sprints.ReleaseSprints;
using Domain.Sprints.ReleaseSprints.ReleaseSprintStates;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace UnitTests
{
    public class ReleaseSprintTests
    {
        [Fact]
        public void TestIfReleaseSprintIsCreatedWithStatusCreated()
        {
            //Arrange
            ReleaseSprint releaseSprint = new ReleaseSprint(1, "", new DateTime(), new DateTime());
            Created status = new Created(releaseSprint);

            //Act

            //Assert
            Assert.IsType(status.GetType(), releaseSprint.Status);

        }
        [Fact]
        public void TestIfReleaseSprintIsInProgress()
        {
            //Arrange
            ReleaseSprint releaseSprint = new ReleaseSprint(1, "", new DateTime(), new DateTime());
            InProgress status = new InProgress(releaseSprint);

            //Act
            releaseSprint.setStatus(new InProgress(releaseSprint));
            //Assert
            Assert.IsType(status.GetType(), releaseSprint.Status);

        }
        [Fact]
        public void TestIfReleaseSprintIsInFinished()
        {
            //Arrange
            ReleaseSprint releaseSprint = new ReleaseSprint(1, "", new DateTime(), new DateTime());
            Finished status = new Finished(releaseSprint);

            //Act
            releaseSprint.setStatus(new Finished(releaseSprint));
            //Assert
            Assert.IsType(status.GetType(), releaseSprint.Status);

        }
        [Fact]
        public void TestIfReleaseSprintIsReleased()
        {
            //Arrange
            ReleaseSprint releaseSprint = new ReleaseSprint(1, "", new DateTime(), new DateTime());
            Released status = new Released(releaseSprint);

            //Act
            releaseSprint.setStatus(new Released(releaseSprint));
            //Assert
            Assert.IsType(status.GetType(), releaseSprint.Status);

        }
        [Fact]
        public void TestIfReleaseSprintIsCompleted()
        {
            //Arrange
            ReleaseSprint releaseSprint = new ReleaseSprint(1, "", new DateTime(), new DateTime());
            Completed status = new Completed(releaseSprint);

            //Act
            releaseSprint.setStatus(new Completed(releaseSprint));
            //Assert
            Assert.IsType(status.GetType(), releaseSprint.Status);

        }
        //Created
        [Fact]
        public void TestIfReleaseSprintBecomesInProgressWhenStartFromCreated()
        {
            //Arrange
            ReleaseSprint releaseSprint = new ReleaseSprint(1, "", new DateTime(), new DateTime());
            InProgress status = new InProgress(releaseSprint);

            //Act
            releaseSprint.Status.Start();
            //Assert
            Assert.IsType(status.GetType(), releaseSprint.Status);

        }
        [Fact]
        public void TestIfReleaseSprintStaysCreatedWhenFinishFromCreated()
        {
            //Arrange
            ReleaseSprint releaseSprint = new ReleaseSprint(1, "", new DateTime(), new DateTime());
            Created status = new Created(releaseSprint);

            //Act
            releaseSprint.Status.Finish();
            //Assert
            Assert.IsType(status.GetType(), releaseSprint.Status);

        }
        [Fact]
        public void TestIfReleaseSprintStaysCreatedWhenReleaseFromCreated()
        {
            //Arrange
            ReleaseSprint releaseSprint = new ReleaseSprint(1, "", new DateTime(), new DateTime());
            Created status = new Created(releaseSprint);

            //Act
            releaseSprint.Status.Release();
            //Assert
            Assert.IsType(status.GetType(), releaseSprint.Status);

        }
        [Fact]
        public void TestIfReleaseSprintStaysCreatedWhenCompleteFromCreated()
        {
            //Arrange
            ReleaseSprint releaseSprint = new ReleaseSprint(1, "", new DateTime(), new DateTime());
            Created status = new Created(releaseSprint);

            //Act
            releaseSprint.Status.Complete();
            //Assert
            Assert.IsType(status.GetType(), releaseSprint.Status);

        }
        //InProgress
        [Fact]
        public void TestIfReleaseSprintBecomesFinishedWhenFinishFromInProgress()
        {
            //Arrange
            ReleaseSprint releaseSprint = new ReleaseSprint(1, "", new DateTime(), new DateTime());
            Finished status = new Finished(releaseSprint);

            //Act
            releaseSprint.Status.Start();
            releaseSprint.Status.Finish();
            //Assert
            Assert.IsType(status.GetType(), releaseSprint.Status);

        }
        [Fact]
        public void TestIfReleaseSprintStaysInProgressWhenStartFromInProgress()
        {
            //Arrange
            ReleaseSprint releaseSprint = new ReleaseSprint(1, "", new DateTime(), new DateTime());
            InProgress status = new InProgress(releaseSprint);

            //Act
            releaseSprint.Status.Start();
            releaseSprint.Status.Start();
            //Assert
            Assert.IsType(status.GetType(), releaseSprint.Status);

        }
        [Fact]
        public void TestIfReleaseSprintStaysInProgressWhenReleaseFromInProgress()
        {
            //Arrange
            ReleaseSprint releaseSprint = new ReleaseSprint(1, "", new DateTime(), new DateTime());
            InProgress status = new InProgress(releaseSprint);

            //Act
            releaseSprint.Status.Start();
            releaseSprint.Status.Release();
            //Assert
            Assert.IsType(status.GetType(), releaseSprint.Status);

        }
        [Fact]
        public void TestIfReleaseSprintStaysInProgressWhenCompleteFromInProgress()
        {
            //Arrange
            ReleaseSprint releaseSprint = new ReleaseSprint(1, "", new DateTime(), new DateTime());
            InProgress status = new InProgress(releaseSprint);

            //Act
            releaseSprint.Status.Start();
            releaseSprint.Status.Complete();
            //Assert
            Assert.IsType(status.GetType(), releaseSprint.Status);

        }
        //Finished
        [Fact]
        public void TestIfReleaseSprintBecomesReleasedWhenReleaseFromFinished()
        {
            //Arrange
            ReleaseSprint releaseSprint = new ReleaseSprint(1, "", new DateTime(), new DateTime());
            Released status = new Released(releaseSprint);

            //Act
            releaseSprint.Status.Start();
            releaseSprint.Status.Finish();
            releaseSprint.Status.Release();
            //Assert
            Assert.IsType(status.GetType(), releaseSprint.Status);

        }
        [Fact]
        public void TestIfReleaseSprintStaysFinishedWhenStartFromFinished()
        {
            //Arrange
            ReleaseSprint releaseSprint = new ReleaseSprint(1, "", new DateTime(), new DateTime());
            Finished status = new Finished(releaseSprint);

            //Act
            releaseSprint.Status.Start();
            releaseSprint.Status.Finish();
            releaseSprint.Status.Start();
            //Assert
            Assert.IsType(status.GetType(), releaseSprint.Status);

        }
        [Fact]
        public void TestIfReleaseSprintStaysFinishedWhenFinishFromFinished()
        {
            //Arrange
            ReleaseSprint releaseSprint = new ReleaseSprint(1, "", new DateTime(), new DateTime());
            Finished status = new Finished(releaseSprint);

            //Act
            releaseSprint.Status.Start();
            releaseSprint.Status.Finish();
            releaseSprint.Status.Finish();
            //Assert
            Assert.IsType(status.GetType(), releaseSprint.Status);

        }
        [Fact]
        public void TestIfReleaseSprintStaysFinishedWhenCompleteFromFinished()
        {
            //Arrange
            ReleaseSprint releaseSprint = new ReleaseSprint(1, "", new DateTime(), new DateTime());
            Finished status = new Finished(releaseSprint);

            //Act
            releaseSprint.Status.Start();
            releaseSprint.Status.Finish();
            releaseSprint.Status.Complete();
            //Assert
            Assert.IsType(status.GetType(), releaseSprint.Status);

        }
        //Released
        [Fact]
        public void TestIfReleaseSprintBecomesCompletedWhenCompleteFromReleased()
        {
            //Arrange
            ReleaseSprint releaseSprint = new ReleaseSprint(1, "", new DateTime(), new DateTime());
            Completed status = new Completed(releaseSprint);

            //Act
            releaseSprint.Status.Start();
            releaseSprint.Status.Finish();
            releaseSprint.Status.Release();
            releaseSprint.Status.Complete();
            //Assert
            Assert.IsType(status.GetType(), releaseSprint.Status);

        }
        [Fact]
        public void TestIfReleaseSprintStaysReleasedWhenStartFromReleased()
        {
            //Arrange
            ReleaseSprint releaseSprint = new ReleaseSprint(1, "", new DateTime(), new DateTime());
            Released status = new Released(releaseSprint);

            //Act
            releaseSprint.Status.Start();
            releaseSprint.Status.Finish();
            releaseSprint.Status.Release();
            releaseSprint.Status.Start();
            //Assert
            Assert.IsType(status.GetType(), releaseSprint.Status);

        }
        [Fact]
        public void TestIfReleaseSprintStaysReleasedWhenFinishFromReleased()
        {
            //Arrange
            ReleaseSprint releaseSprint = new ReleaseSprint(1, "", new DateTime(), new DateTime());
            Released status = new Released(releaseSprint);

            //Act
            releaseSprint.Status.Start();
            releaseSprint.Status.Finish();
            releaseSprint.Status.Release();
            releaseSprint.Status.Finish();
            //Assert
            Assert.IsType(status.GetType(), releaseSprint.Status);

        }
        [Fact]
        public void TestIfReleaseSprintStaysReleasedWhenReleaseFromReleased()
        {
            //Arrange
            ReleaseSprint releaseSprint = new ReleaseSprint(1, "", new DateTime(), new DateTime());
            Released status = new Released(releaseSprint);

            //Act
            releaseSprint.Status.Start();
            releaseSprint.Status.Finish();
            releaseSprint.Status.Release();
            releaseSprint.Status.Release();
            //Assert
            Assert.IsType(status.GetType(), releaseSprint.Status);

        }
        //Completed
        [Fact]
        public void TestIfReleaseSprintStaysCompletedWhenStartFromCompleted()
        {
            //Arrange
            ReleaseSprint releaseSprint = new ReleaseSprint(1, "", new DateTime(), new DateTime());
            Completed status = new Completed(releaseSprint);

            //Act
            releaseSprint.Status.Start();
            releaseSprint.Status.Finish();
            releaseSprint.Status.Release();
            releaseSprint.Status.Complete();
            releaseSprint.Status.Start();
            //Assert
            Assert.IsType(status.GetType(), releaseSprint.Status);

        }
        [Fact]
        public void TestIfReleaseSprintStaysCompletedWhenFinishFromCompleted()
        {
            //Arrange
            ReleaseSprint releaseSprint = new ReleaseSprint(1, "", new DateTime(), new DateTime());
            Completed status = new Completed(releaseSprint);

            //Act
            releaseSprint.Status.Start();
            releaseSprint.Status.Finish();
            releaseSprint.Status.Release();
            releaseSprint.Status.Complete();
            releaseSprint.Status.Finish();
            //Assert
            Assert.IsType(status.GetType(), releaseSprint.Status);

        }
        [Fact]
        public void TestIfReleaseSprintStaysCompletedWhenReleaseFromCompleted()
        {
            //Arrange
            ReleaseSprint releaseSprint = new ReleaseSprint(1, "", new DateTime(), new DateTime());
            Completed status = new Completed(releaseSprint);

            //Act
            releaseSprint.Status.Start();
            releaseSprint.Status.Finish();
            releaseSprint.Status.Release();
            releaseSprint.Status.Complete();
            releaseSprint.Status.Release();
            //Assert
            Assert.IsType(status.GetType(), releaseSprint.Status);

        }
        [Fact]
        public void TestIfReleaseSprintStaysCompletedWhenCompleteFromCompleted()
        {
            //Arrange
            ReleaseSprint releaseSprint = new ReleaseSprint(1, "", new DateTime(), new DateTime());
            Completed status = new Completed(releaseSprint);

            //Act
            releaseSprint.Status.Start();
            releaseSprint.Status.Finish();
            releaseSprint.Status.Release();
            releaseSprint.Status.Complete();
            releaseSprint.Status.Complete();
            //Assert
            Assert.IsType(status.GetType(), releaseSprint.Status);

        }
        [Fact]
        public void TestIfReleaseSprintCanHaveMoreThanOneItem() 
        {
            //Arrange
            ReleaseSprint releaseSprint = new ReleaseSprint(1,"",new DateTime(),new DateTime());
            Item item = new Item(1, "");
            Item item2 = new Item(2, "");
            //Act
            releaseSprint.addItem(item);
            releaseSprint.addItem(item2);
            //Assert
            Assert.Equal(item, releaseSprint.ItemList[0]);
            Assert.Equal(item2, releaseSprint.ItemList[1]);
        }
        [Fact]
        public void TestIfNameCanBeChangedWhenReleaseSprintIsNotStarted()
        {
            //Arrange
            ReleaseSprint releaseSprint = new ReleaseSprint(1, "First", new DateTime(), new DateTime());

            //Act
            releaseSprint.SetName("Second");
            releaseSprint.setStatus(new Finished(releaseSprint));
            releaseSprint.SetName("Third");
            //Assert
            Assert.Equal("Second", releaseSprint.Name);
        }
        [Fact]
        public void TestIfStartDateCanBeChangedWhenReleaseSprintIsNotStarted()
        {
            //Arrange
            ReleaseSprint releaseSprint = new ReleaseSprint(1, "First", new DateTime(), new DateTime());

            //Act
            releaseSprint.SetStartDate(new DateTime(2022,3,25));
            releaseSprint.setStatus(new Finished(releaseSprint));
            releaseSprint.SetStartDate(new DateTime(2022, 3, 24));
            //Assert
            Assert.Equal(new DateTime(2022,3,25), releaseSprint.StartDate);
        }
        [Fact]
        public void TestIfEndDateCanBeChangedWhenReleaseSprintIsNotStarted()
        {
            //Arrange
            ReleaseSprint releaseSprint = new ReleaseSprint(1, "First", new DateTime(), new DateTime());

            //Act
            releaseSprint.SetEndDate(new DateTime(2022, 3, 25));
            releaseSprint.setStatus(new Finished(releaseSprint));
            releaseSprint.SetEndDate(new DateTime(2022, 3, 24));
            //Assert
            Assert.Equal(new DateTime(2022, 3, 25), releaseSprint.EndDate);
        }
        [Fact]
        public void TestIfPipelineCanBeStartedWhenFinished() 
        {
            StringWriter sw = new StringWriter();
            Console.SetOut(sw);

            //Arrange
            ReleaseSprint sprint = new ReleaseSprint(0, "", new DateTime(), new DateTime());
            sprint.setStatus(new Finished(sprint));
            Pipeline pipeline = new Pipeline("TestPipeline");
            sprint.Pipeline = pipeline;
            Group group = new Group("Test");
            sprint.Pipeline.AddComponent(group);

            group.AddComponent(new Domain.Pipeline.Action("TestCode"));
            group.AddComponent(new Domain.Pipeline.Action("TestCode2"));

            ConcreteVisitor concreteVisitor = new ConcreteVisitor();
            StringBuilder sb = sw.GetStringBuilder();
            sb.Remove(0, sb.Length);
            //Act
            sprint.StartPipeline((object)concreteVisitor, 1);

            //Assert
            Assert.Equal("The pipeline has started\n---TestPipeline---\n-Test\n-----TestCode\n-----TestCode2\n", sw.ToString());
            StreamWriter standardOutput = new StreamWriter(Console.OpenStandardOutput());
            standardOutput.AutoFlush = true;
            Console.SetOut(standardOutput);
        }
        [Fact]
        public void TestIfPipelineCantBeStartedWhenNotFinished()
        {
            StringWriter sw = new StringWriter();
            Console.SetOut(sw);

            //Arrange
            ReleaseSprint sprint = new ReleaseSprint(0, "", new DateTime(), new DateTime());
            
            Pipeline pipeline = new Pipeline("TestPipeline");
            sprint.Pipeline = pipeline;
            Group group = new Group("Test");
            sprint.Pipeline.AddComponent(group);

            group.AddComponent(new Domain.Pipeline.Action("TestCode"));
            group.AddComponent(new Domain.Pipeline.Action("TestCode2"));

            ConcreteVisitor concreteVisitor = new ConcreteVisitor();
            StringBuilder sb = sw.GetStringBuilder();
            sb.Remove(0, sb.Length);
            //Act
            sprint.StartPipeline((object)concreteVisitor, 1);

            //Assert
            Assert.Equal("Cant execute pipeline\n", sw.ToString());
            StreamWriter standardOutput = new StreamWriter(Console.OpenStandardOutput());
            standardOutput.AutoFlush = true;
            Console.SetOut(standardOutput);
        }
        
    }
}
