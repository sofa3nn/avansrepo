﻿using Domain.Sprints.ReleaseSprints;
using Domain.Sprints.ReviewSprints;
using Domain.Report;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain
{
    public abstract class Sprint
    {
        public int Id { get; set; }
        public string Name {get; set; }
        
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public Director Director { get; set; }
        public List<Item> ItemList { get; private set; }
        public Pipeline.Pipeline Pipeline { get; set; }
        public Sprint(int id, string name, DateTime start, DateTime end)
        {
            Id = id;
            Name = name;
            StartDate = start;
            EndDate = end;
            ItemList = new List<Item>();
        }

        public void SetName(String name)
        {
            if (IsNotCompleted())
            {
                Name = name;
            }
        }

        public void SetStartDate(DateTime startDate)
        {
            if (IsNotCompleted())
            {
                StartDate = startDate;
            }
        }
        public void SetEndDate(DateTime endDate)
        {
            if (IsNotCompleted())
            {
                EndDate = endDate;
            }
        }

        public void addItem(Item item)
        {
            ItemList.Add(item);
        }
        public void StartPipeline(Object obj, int depth) 
        {
            if (GetType() == new ReviewSprint(1, "", new DateTime(), new DateTime()).GetType())
            {

                if (GetStatus().GetType() == new Sprints.ReviewSprints.ReviewSprintStates.Finished((ReviewSprint)this).GetType())
                {
                    Pipeline?.Start(obj, depth);
                }
                else
                {
                    Console.WriteLine("Cant execute pipeline");
                }
            }
            else 
            {
                if (GetStatus().GetType() == new Sprints.ReleaseSprints.ReleaseSprintStates.Finished((ReleaseSprint)this).GetType())
                {
                    Pipeline?.Start(obj, depth);
                }
                else
                {
                    Console.WriteLine("Cant execute pipeline");
                }
            }

               
        }
        public abstract Object GetStatus();

        public bool IsNotCompleted()
        {
            if (GetType() == new ReviewSprint(1, "", new DateTime(), new DateTime()).GetType())
            {

                if (GetStatus().GetType() == new Sprints.ReviewSprints.ReviewSprintStates.Created((ReviewSprint)this).GetType())
                {
                    return true;
                }
                else
                {
                    return false;
                }
            } else
            {
                if (GetStatus().GetType() == new Sprints.ReleaseSprints.ReleaseSprintStates.Created((ReleaseSprint)this).GetType())
                {
                    return true;
                } else
                {
                    return false;
                }
            }

        }
    }
}
