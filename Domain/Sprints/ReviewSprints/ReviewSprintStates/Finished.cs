﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Sprints.ReviewSprints.ReviewSprintStates
{
    public class Finished : ReviewSprintStatus
    {
        public Finished(ReviewSprint reviewSprint) : base(reviewSprint)
        {
        }

        public override void Start()
        {
            Console.WriteLine("The sprint has already been started.");
        }

        public override void Finish()
        {
            Console.WriteLine("The sprint has already been finished.");
        }

        public override void Review()
        {
            ReviewSprint.setStatus(new Reviewed(ReviewSprint));
            Console.WriteLine("The sprint is being reviewed.");
        }

        public override void Complete()
        {
            Console.WriteLine("The sprint has to be reviewed before completing.");
        }
    }
}
