﻿using Domain.Comments.Visitors;
using Domain.Composites;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Comments
{
    public class MessageGroup : CompositeComponent
    {
        public override void AcceptVisitor(Object obj, int depth)
        {
            Visitor? visitor = obj as Visitor;
            visitor?.VisitMessageGroup(this);
            base.AcceptVisitor(visitor, depth);
        }
    }
}
