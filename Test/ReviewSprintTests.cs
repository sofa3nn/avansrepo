﻿using Domain;
using Domain.Pipeline;
using Domain.Pipeline.Visitors;
using Domain.Sprints.ReviewSprints;
using Domain.Sprints.ReviewSprints.ReviewSprintStates;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace UnitTests
{
    public class ReviewSprintTests
    {
        [Fact]
        public void TestIfReviewSprintIsCreatedWithStatusCreated() 
        {
            //Arrange
            ReviewSprint reviewSprint = new ReviewSprint(1, "", new DateTime(), new DateTime());
            Created status = new Created(reviewSprint);

            //Act

            //Assert
            Assert.IsType(status.GetType(), reviewSprint.Status);

        }
        [Fact]
        public void TestIfReviewSprintIsInProgress()
        {
            //Arrange
            ReviewSprint reviewSprint = new ReviewSprint(1, "", new DateTime(), new DateTime());
            InProgress status = new InProgress(reviewSprint);

            //Act
            reviewSprint.setStatus(new InProgress(reviewSprint));
            //Assert
            Assert.IsType(status.GetType(), reviewSprint.Status);

        }
        [Fact]
        public void TestIfReviewSprintIsInFinished()
        {
            //Arrange
            ReviewSprint reviewSprint = new ReviewSprint(1, "", new DateTime(), new DateTime());
            Finished status = new Finished(reviewSprint);

            //Act
            reviewSprint.setStatus(new Finished(reviewSprint));
            //Assert
            Assert.IsType(status.GetType(), reviewSprint.Status);

        }
        [Fact]
        public void TestIfReviewSprintIsReviewed()
        {
            //Arrange
            ReviewSprint reviewSprint = new ReviewSprint(1, "", new DateTime(), new DateTime());
            Reviewed status = new Reviewed(reviewSprint);

            //Act
            reviewSprint.setStatus(new Reviewed(reviewSprint));
            //Assert
            Assert.IsType(status.GetType(), reviewSprint.Status);

        }
        [Fact]
        public void TestIfReviewSprintIsCompleted()
        {
            //Arrange
            ReviewSprint reviewSprint = new ReviewSprint(1, "", new DateTime(), new DateTime());
            Completed status = new Completed(reviewSprint);

            //Act
            reviewSprint.setStatus(new Completed(reviewSprint));
            //Assert
            Assert.IsType(status.GetType(), reviewSprint.Status);

        }
        //Created
        [Fact]
        public void TestIfReviewSprintBecomesInProgressWhenStartFromCreated()
        {
            //Arrange
            ReviewSprint ReviewSprint = new ReviewSprint(1, "", new DateTime(), new DateTime());
            InProgress status = new InProgress(ReviewSprint);

            //Act
            ReviewSprint.Status.Start();
            //Assert
            Assert.IsType(status.GetType(), ReviewSprint.Status);

        }
        [Fact]
        public void TestIfReviewSprintStaysCreatedWhenFinishFromCreated()
        {
            //Arrange
            ReviewSprint ReviewSprint = new ReviewSprint(1, "", new DateTime(), new DateTime());
            Created status = new Created(ReviewSprint);

            //Act
            ReviewSprint.Status.Finish();
            //Assert
            Assert.IsType(status.GetType(), ReviewSprint.Status);

        }
        [Fact]
        public void TestIfReviewSprintStaysCreatedWhenReviewFromCreated()
        {
            //Arrange
            ReviewSprint ReviewSprint = new ReviewSprint(1, "", new DateTime(), new DateTime());
            Created status = new Created(ReviewSprint);

            //Act
            ReviewSprint.Status.Review();
            //Assert
            Assert.IsType(status.GetType(), ReviewSprint.Status);

        }
        [Fact]
        public void TestIfReviewSprintStaysCreatedWhenCompleteFromCreated()
        {
            //Arrange
            ReviewSprint ReviewSprint = new ReviewSprint(1, "", new DateTime(), new DateTime());
            Created status = new Created(ReviewSprint);

            //Act
            ReviewSprint.Status.Complete();
            //Assert
            Assert.IsType(status.GetType(), ReviewSprint.Status);

        }
        //InProgress
        [Fact]
        public void TestIfReviewSprintBecomesFinishedWhenFinishFromInProgress()
        {
            //Arrange
            ReviewSprint ReviewSprint = new ReviewSprint(1, "", new DateTime(), new DateTime());
            Finished status = new Finished(ReviewSprint);

            //Act
            ReviewSprint.Status.Start();
            ReviewSprint.Status.Finish();
            //Assert
            Assert.IsType(status.GetType(), ReviewSprint.Status);

        }
        [Fact]
        public void TestIfReviewSprintStaysInProgressWhenStartFromInProgress()
        {
            //Arrange
            ReviewSprint ReviewSprint = new ReviewSprint(1, "", new DateTime(), new DateTime());
            InProgress status = new InProgress(ReviewSprint);

            //Act
            ReviewSprint.Status.Start();
            ReviewSprint.Status.Start();
            //Assert
            Assert.IsType(status.GetType(), ReviewSprint.Status);

        }
        [Fact]
        public void TestIfReviewSprintStaysInProgressWhenReviewFromInProgress()
        {
            //Arrange
            ReviewSprint ReviewSprint = new ReviewSprint(1, "", new DateTime(), new DateTime());
            InProgress status = new InProgress(ReviewSprint);

            //Act
            ReviewSprint.Status.Start();
            ReviewSprint.Status.Review();
            //Assert
            Assert.IsType(status.GetType(), ReviewSprint.Status);

        }
        [Fact]
        public void TestIfReviewSprintStaysInProgressWhenCompleteFromInProgress()
        {
            //Arrange
            ReviewSprint ReviewSprint = new ReviewSprint(1, "", new DateTime(), new DateTime());
            InProgress status = new InProgress(ReviewSprint);

            //Act
            ReviewSprint.Status.Start();
            ReviewSprint.Status.Complete();
            //Assert
            Assert.IsType(status.GetType(), ReviewSprint.Status);

        }
        //Finished
        [Fact]
        public void TestIfReviewSprintBecomesReviewdWhenReviewFromFinished()
        {
            //Arrange
            ReviewSprint ReviewSprint = new ReviewSprint(1, "", new DateTime(), new DateTime());
            Reviewed status = new Reviewed(ReviewSprint);

            //Act
            ReviewSprint.Status.Start();
            ReviewSprint.Status.Finish();
            ReviewSprint.Status.Review();
            //Assert
            Assert.IsType(status.GetType(), ReviewSprint.Status);

        }
        [Fact]
        public void TestIfReviewSprintStaysFinishedWhenStartFromFinished()
        {
            //Arrange
            ReviewSprint ReviewSprint = new ReviewSprint(1, "", new DateTime(), new DateTime());
            Finished status = new Finished(ReviewSprint);

            //Act
            ReviewSprint.Status.Start();
            ReviewSprint.Status.Finish();
            ReviewSprint.Status.Start();
            //Assert
            Assert.IsType(status.GetType(), ReviewSprint.Status);

        }
        [Fact]
        public void TestIfReviewSprintStaysFinishedWhenFinishFromFinished()
        {
            //Arrange
            ReviewSprint ReviewSprint = new ReviewSprint(1, "", new DateTime(), new DateTime());
            Finished status = new Finished(ReviewSprint);

            //Act
            ReviewSprint.Status.Start();
            ReviewSprint.Status.Finish();
            ReviewSprint.Status.Finish();
            //Assert
            Assert.IsType(status.GetType(), ReviewSprint.Status);

        }
        [Fact]
        public void TestIfReviewSprintStaysFinishedWhenCompleteFromFinished()
        {
            //Arrange
            ReviewSprint ReviewSprint = new ReviewSprint(1, "", new DateTime(), new DateTime());
            Finished status = new Finished(ReviewSprint);

            //Act
            ReviewSprint.Status.Start();
            ReviewSprint.Status.Finish();
            ReviewSprint.Status.Complete();
            //Assert
            Assert.IsType(status.GetType(), ReviewSprint.Status);

        }
        //Reviewd
        [Fact]
        public void TestIfReviewSprintBecomesCompletedWhenCompleteFromReviewd()
        {
            //Arrange
            ReviewSprint ReviewSprint = new ReviewSprint(1, "", new DateTime(), new DateTime());
            ReviewSprint.SummaryDocument = "Test";
            Completed status = new Completed(ReviewSprint);

            //Act
            ReviewSprint.Status.Start();
            ReviewSprint.Status.Finish();
            ReviewSprint.Status.Review();
            ReviewSprint.Status.Complete();
            //Assert
            Assert.IsType(status.GetType(), ReviewSprint.Status);

        }
        [Fact]
        public void TestIfReviewSprintStaysReviewdWhenStartFromReviewd()
        {
            //Arrange
            ReviewSprint ReviewSprint = new ReviewSprint(1, "", new DateTime(), new DateTime());
            Reviewed status = new Reviewed(ReviewSprint);

            //Act
            ReviewSprint.Status.Start();
            ReviewSprint.Status.Finish();
            ReviewSprint.Status.Review();
            ReviewSprint.Status.Start();
            //Assert
            Assert.IsType(status.GetType(), ReviewSprint.Status);

        }
        [Fact]
        public void TestIfReviewSprintStaysReviewdWhenFinishFromReviewd()
        {
            //Arrange
            ReviewSprint ReviewSprint = new ReviewSprint(1, "", new DateTime(), new DateTime());
            Reviewed status = new Reviewed(ReviewSprint);

            //Act
            ReviewSprint.Status.Start();
            ReviewSprint.Status.Finish();
            ReviewSprint.Status.Review();
            ReviewSprint.Status.Finish();
            //Assert
            Assert.IsType(status.GetType(), ReviewSprint.Status);

        }
        [Fact]
        public void TestIfReviewSprintStaysReviewdWhenReviewFromReviewd()
        {
            //Arrange
            ReviewSprint ReviewSprint = new ReviewSprint(1, "", new DateTime(), new DateTime());
            Reviewed status = new Reviewed(ReviewSprint);

            //Act
            ReviewSprint.Status.Start();
            ReviewSprint.Status.Finish();
            ReviewSprint.Status.Review();
            ReviewSprint.Status.Review();
            //Assert
            Assert.IsType(status.GetType(), ReviewSprint.Status);

        }
        //Completed
        [Fact]
        public void TestIfReviewSprintStaysCompletedWhenStartFromCompleted()
        {
            //Arrange
            ReviewSprint ReviewSprint = new ReviewSprint(1, "", new DateTime(), new DateTime());
            ReviewSprint.SummaryDocument = "Test";
            Completed status = new Completed(ReviewSprint);

            //Act
            ReviewSprint.Status.Start();
            ReviewSprint.Status.Finish();
            ReviewSprint.Status.Review();
            ReviewSprint.Status.Complete();
            ReviewSprint.Status.Start();
            //Assert
            Assert.IsType(status.GetType(), ReviewSprint.Status);

        }
        [Fact]
        public void TestIfReviewSprintStaysCompletedWhenFinishFromCompleted()
        {
            //Arrange
            ReviewSprint ReviewSprint = new ReviewSprint(1, "", new DateTime(), new DateTime());
            ReviewSprint.SummaryDocument = "Test";
            Completed status = new Completed(ReviewSprint);

            //Act
            ReviewSprint.Status.Start();
            ReviewSprint.Status.Finish();
            ReviewSprint.Status.Review();
            ReviewSprint.Status.Complete();
            ReviewSprint.Status.Finish();
            //Assert
            Assert.IsType(status.GetType(), ReviewSprint.Status);

        }
        [Fact]
        public void TestIfReviewSprintStaysCompletedWhenReviewFromCompleted()
        {
            //Arrange
            ReviewSprint ReviewSprint = new ReviewSprint(1, "", new DateTime(), new DateTime());
            ReviewSprint.SummaryDocument = "Test";
            Completed status = new Completed(ReviewSprint);

            //Act
            ReviewSprint.Status.Start();
            ReviewSprint.Status.Finish();
            ReviewSprint.Status.Review();
            ReviewSprint.Status.Complete();
            ReviewSprint.Status.Review();
            //Assert
            Assert.IsType(status.GetType(), ReviewSprint.Status);

        }
        [Fact]
        public void TestIfReviewSprintStaysCompletedWhenCompleteFromCompleted()
        {
            //Arrange
            ReviewSprint ReviewSprint = new ReviewSprint(1, "", new DateTime(), new DateTime());
            ReviewSprint.SummaryDocument = "Test";
            Completed status = new Completed(ReviewSprint);

            //Act
            ReviewSprint.Status.Start();
            ReviewSprint.Status.Finish();
            ReviewSprint.Status.Review();
            ReviewSprint.Status.Complete();
            ReviewSprint.Status.Complete();
            //Assert
            Assert.IsType(status.GetType(), ReviewSprint.Status);

        }
        [Fact]
        public void TestIfReviewSprintCanHaveMoreThanOneItem()
        {
            //Arrange
            ReviewSprint reviewSprint = new ReviewSprint(1, "", new DateTime(), new DateTime());
            Item item = new Item(1, "");
            Item item2 = new Item(2, "");
            //Act
            reviewSprint.addItem(item);
            reviewSprint.addItem(item2);
            //Assert
            Assert.Equal(item, reviewSprint.ItemList[0]);
            Assert.Equal(item2, reviewSprint.ItemList[1]);
        }
        [Fact]
        public void TestIfNameCanBeChangedWhenReleaseSprintIsNotStarted()
        {
            //Arrange
            ReviewSprint reviewSprint = new ReviewSprint(1, "First", new DateTime(), new DateTime());

            //Act
            reviewSprint.SetName("Second");
            reviewSprint.setStatus(new Finished(reviewSprint));
            reviewSprint.SetName("Third");
            //Assert
            Assert.Equal("Second", reviewSprint.Name);
        }
        [Fact]
        public void TestIfStartDateCanBeChangedWhenReleaseSprintIsNotStarted()
        {
            //Arrange
            ReviewSprint reviewSprint = new ReviewSprint(1, "First", new DateTime(), new DateTime());

            //Act
            reviewSprint.SetStartDate(new DateTime(2022, 3, 25));
            reviewSprint.setStatus(new Finished(reviewSprint));
            reviewSprint.SetStartDate(new DateTime(2022, 3, 24));
            //Assert
            Assert.Equal(new DateTime(2022, 3, 25), reviewSprint.StartDate);
        }
        [Fact]
        public void TestIfEndDateCanBeChangedWhenReleaseSprintIsNotStarted()
        {
            //Arrange
            ReviewSprint reviewSprint = new ReviewSprint(1, "First", new DateTime(), new DateTime());

            //Act
            reviewSprint.SetEndDate(new DateTime(2022, 3, 25));
            reviewSprint.setStatus(new Finished(reviewSprint));
            reviewSprint.SetEndDate(new DateTime(2022, 3, 24));
            //Assert
            Assert.Equal(new DateTime(2022, 3, 25), reviewSprint.EndDate);
        }
        [Fact]
        public void TestIfPipelineCanBeStartedWhenFinished()
        {
            StringWriter sw = new StringWriter();
            Console.SetOut(sw);

            //Arrange
            ReviewSprint sprint = new ReviewSprint(0, "", new DateTime(), new DateTime());
            sprint.setStatus(new Finished(sprint));
            Pipeline pipeline = new Pipeline("TestPipeline");
            sprint.Pipeline = pipeline;
            Group group = new Group("Test");
            sprint.Pipeline.AddComponent(group);

            group.AddComponent(new Domain.Pipeline.Action("TestCode"));
            group.AddComponent(new Domain.Pipeline.Action("TestCode2"));

            ConcreteVisitor concreteVisitor = new ConcreteVisitor();
            StringBuilder sb = sw.GetStringBuilder();
            sb.Remove(0, sb.Length);
            //Act
            sprint.StartPipeline((object)concreteVisitor, 1);

            //Assert
            Assert.Equal("The pipeline has started\n---TestPipeline---\n-Test\n-----TestCode\n-----TestCode2\n", sw.ToString());
            StreamWriter standardOutput = new StreamWriter(Console.OpenStandardOutput());
            standardOutput.AutoFlush = true;
            Console.SetOut(standardOutput);
        }
        [Fact]
        public void TestIfPipelineCantBeStartedWhenNotFinished()
        {
            StringWriter sw = new StringWriter();
            Console.SetOut(sw);

            //Arrange
            ReviewSprint sprint = new ReviewSprint(0, "", new DateTime(), new DateTime());

            Pipeline pipeline = new Pipeline("TestPipeline");
            sprint.Pipeline = pipeline;
            Group group = new Group("Test");
            sprint.Pipeline.AddComponent(group);

            group.AddComponent(new Domain.Pipeline.Action("TestCode"));
            group.AddComponent(new Domain.Pipeline.Action("TestCode2"));

            ConcreteVisitor concreteVisitor = new ConcreteVisitor();
            StringBuilder sb = sw.GetStringBuilder();
            sb.Remove(0, sb.Length);
            //Act
            sprint.StartPipeline((object)concreteVisitor, 1);

            //Assert
            Assert.Equal("Cant execute pipeline\n", sw.ToString());
            StreamWriter standardOutput = new StreamWriter(Console.OpenStandardOutput());
            standardOutput.AutoFlush = true;
            Console.SetOut(standardOutput);
        }
        [Fact]
        public void TestIfSprintCanBeCompletedWhenDocumentIsAdded() 
        {
            //Arrange
            ReviewSprint reviewSprint = new ReviewSprint(1, "First", new DateTime(), new DateTime());
            //Act
            reviewSprint.setStatus(new Reviewed(reviewSprint));
            reviewSprint.SummaryDocument = "TestDocument";
            reviewSprint.Status.Complete();
            //Assert
            Assert.IsType((new Completed(reviewSprint)).GetType(), reviewSprint.Status);
        }
        [Fact]
        public void TestIfSprintCantBeCompletedWhenDocumentIsNotAdded()
        {
            //Arrange
            ReviewSprint reviewSprint = new ReviewSprint(1, "First", new DateTime(), new DateTime());
            //Act
            reviewSprint.setStatus(new Reviewed(reviewSprint));
            
            reviewSprint.Status.Complete();
            //Assert
            Assert.IsType((new Reviewed(reviewSprint)).GetType(), reviewSprint.Status);
        }
    }
}
