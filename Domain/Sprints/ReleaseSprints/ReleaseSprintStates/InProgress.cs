﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Sprints.ReleaseSprints.ReleaseSprintStates
{
    public class InProgress : ReleaseSprintStatus
    {
        public InProgress(ReleaseSprint releaseSprint) : base(releaseSprint)
        {
        }

        public override void Start()
        {
            Console.WriteLine("The sprint has already been started.");
        }

        public override void Finish()
        {
            ReleaseSprint.setStatus(new Finished(ReleaseSprint));
            Console.WriteLine("The sprint is finished.");
        }

        public override void Release()
        {
            Console.WriteLine("The sprint has to be finished before releasing.");
        }

        public override void Complete()
        {
            Console.WriteLine("The sprint has to be finished before completing.");
        }

    }
}
