﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Sprints.ReleaseSprints.ReleaseSprintStates
{
    public class Completed : ReleaseSprintStatus
    {
        public Completed(ReleaseSprint releaseSprint) : base(releaseSprint)
        {
        }

        public override void Start()
        {
            Console.WriteLine("The sprint has already been started.");
        }

        public override void Finish()
        {
            Console.WriteLine("The sprint has already been finished.");
        }

        public override void Release()
        {
            Console.WriteLine("The sprint has already been released.");
        }

        public override void Complete()
        {
            Console.WriteLine("The sprint has already been completed.");
        }
    }
}
