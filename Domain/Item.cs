﻿using Domain.ItemStates;
using Domain.Observer;
using Domain.Persons;
using Domain.Comments;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Domain.Composites;

namespace Domain
{
    public class Item : Publisher
    {
        public int Id { get; set; }

        public string? Description { get; set; }

        public Developer? Developer { get; set; }

        public List<Activity> Activities { get; set; }

        public ItemStatus Status { get; set; }

        public List<Subscriber> Subscribers = new List<Subscriber>();

        public MessageGroup MessageGroup = new MessageGroup();
        public bool NewMessage = false;

        public Item(int id, string? description)
        {
            Id = id;
            Description = description;
            Activities = new List<Activity>();
            Status = new ToDo(this);
        }

        public void setStatus(ItemStatus status)
        {
            Status = status;
        }
        public void ChangeComment(string NewComment, Comment comment) 
        {
            if (Status.GetType() != new Done(this).GetType()) 
            { 
            comment.ChangeText(NewComment);
            }
            else
            {
                Console.WriteLine("Item is done, you can't change your comment");
            }

        }
        public void AddComment(Comment NewComment, Comment? comment) 
        {
            
            if (Status.GetType() != new Done(this).GetType())
            {
                if (comment != null)
                {
                    comment.AddComponent(NewComment);
                    NewMessage = true;
                    publish();
                }
                else
                {
                    MessageGroup.AddComponent(NewComment);
                }
            }
            else
            {
                Console.WriteLine("Item is done, you can't comment");
            }
           
        }
        public void AddActivity(Activity activity)
        {
            Activities.Add(activity);
        }

        public void PickupItem(Developer developer)
        {
            Developer = developer;
            
        }

        public void subscribe(Subscriber subscriber)
        {
            Subscribers.Add(subscriber);
        }

        public void unsubscribe(Subscriber subscriber)
        {
            Subscribers.Remove(subscriber);
        }

        public void publish()
        {
            foreach (Subscriber subscriber in Subscribers)
            {
                subscriber.updateItem(this.Status, NewMessage);
            }
            NewMessage = false;
        }
    }
}
