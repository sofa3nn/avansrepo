﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.ItemStates
{
    //State Pattern
    public abstract class ItemStatus
    {
        public Item Item { get; set; }

        public ItemStatus(Item item)
        {
            Item = item;
        }

        public abstract void ChooseItem();
        public abstract void Finish();
        public abstract void Test();
        public abstract void ReDo();
        public abstract void FinishedTesting();
        public abstract void Complete();



    }
}
