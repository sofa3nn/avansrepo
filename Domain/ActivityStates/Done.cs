﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.ActivityStates
{
    public class Done : ActivityStatus
    {
        public Done(Activity activity) : base(activity)
        {
        }

        public override void GiveUp()
        {
            Console.WriteLine("This Activity is finished.");
        }

        public override void PickUp()
        {
            Console.WriteLine("This Activity is already finished.");
        }

        public override void Finished()
        {
            Console.WriteLine("This Activity is already finished.");
        }
    }
}
