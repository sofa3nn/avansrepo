﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Report
{
    public class Report
    {
        public string Header { get; set; }
        public string Title { get; set; }
        public string Content { get; set; }
        public string Footer { get; set; }
        public Export Export { get; set; }

        public void ExportReport()
        {
            Export.Export(this);

        }
        public void DisplayReport()
        {
            Console.WriteLine("Title :" + Title);
            Console.WriteLine("Header :" + Header);
            Console.WriteLine("Content :" + Content);
            Console.WriteLine("Footer :" + Footer);
        }
    }
}
