﻿using Domain.Composites;
using Domain.Observer;
using Domain.Pipeline.Visitors;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Pipeline
{
    public class Pipeline : CompositeComponent, Publisher
    {

        public string Name { get; set; }
        public List<Subscriber> Subscribers = new List<Subscriber>();

        public Pipeline(string name)
        {
            Name = name;
        }
        public void Start(Object obj, int depth) 
        {
            Console.WriteLine("The pipeline has started");
            AcceptVisitor(obj, depth);
        }
        public void Complete() 
        {
            Console.WriteLine("The pipeline is complete");
            publish();
        }
        public void Failed() 
        {
            Console.WriteLine("The pipeline has failed");
            publish();
        }
        public void Canceled()
        {
            Console.WriteLine("The pipeline has been Canceled");
            publish();
        }
        //Composite
        //Visitor
        public override void AcceptVisitor(Object obj, int depth)
        {
            Visitor? visitor = obj as Visitor;
            visitor?.VisitPipeline(this);
            base.AcceptVisitor(visitor, depth);
        }

        public void subscribe(Subscriber subscriber)
        {
            Subscribers.Add(subscriber);
        }

        public void unsubscribe(Subscriber subscriber)
        {
            Subscribers.Remove(subscriber);
        }

        public void publish()
        {
            foreach (Subscriber subscriber in Subscribers)
            {
                subscriber.updatePipeline(this.Name);
            }
        }
    }
}
