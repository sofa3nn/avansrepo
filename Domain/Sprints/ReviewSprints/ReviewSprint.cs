﻿using Domain.Observer;
using Domain.Sprints.ReviewSprints.ReviewSprintStates;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Sprints.ReviewSprints
{
    public class ReviewSprint : Sprint, Publisher
    {
        public ReviewSprintStatus Status { get; set; }
        public List<Subscriber> Subscribers = new List<Subscriber>();
        public string SummaryDocument { get; set; }
        public ReviewSprint(int id, string name, DateTime start, DateTime end) : base(id, name, start, end)
        {
            Status = new Created(this);
        }
        public void AddDocument(string document) 
        {
            document = SummaryDocument;
        }
        public void setStatus(ReviewSprintStatus status)
        {
            Status = status;
            publish();
        }

        public void subscribe(Subscriber subscriber)
        {
            Subscribers.Add(subscriber);
        }

        public void unsubscribe(Subscriber subscriber)
        {
            Subscribers.Remove(subscriber);
        }

        public void publish()
        {
            foreach (Subscriber subscriber in Subscribers)
            {
                subscriber.updateReviewSprint(Status);
            }
        }

        public override object GetStatus()
        {
            return Status;
        }
    }
}
