﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Sprints.ReleaseSprints.ReleaseSprintStates
{
    public abstract class ReleaseSprintStatus
    {
        public ReleaseSprint ReleaseSprint { get; set; }

        public ReleaseSprintStatus(ReleaseSprint releaseSprint)
        {
            ReleaseSprint = releaseSprint;   
        }

        public abstract void Start();
        public abstract void Finish();
        public abstract void Release();
        public abstract void Complete();
    }
}
