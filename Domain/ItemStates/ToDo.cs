﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.ItemStates
{
    public class ToDo : ItemStatus
    {
        public ToDo(Item item) : base(item)
        {
        }

        public override void ChooseItem()
        {
            Item.setStatus(new Doing(Item));
            Console.WriteLine("Item is chosen.");
        }

        public override void Finish()
        {
            Console.WriteLine("An item has to be chosen first.");
        }

        public override void Test()
        {
            Console.WriteLine("An item has to be finished first.");
        }

        public override void ReDo()
        {
            Console.WriteLine("An item has to be tested first.");
        }

        public override void FinishedTesting()
        {
            Console.WriteLine("An item has to be tested first.");
        }
        
        public override void Complete()
        {
            Console.WriteLine("An item has to be testd first.");
        }

    }
}
