﻿using Domain.Pipeline.Visitors;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Composites
{
    public abstract class Component
    {
        public abstract void AcceptVisitor(Object visitor, int depth);
    }
}
