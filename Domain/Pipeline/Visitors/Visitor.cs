﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Pipeline.Visitors
{
    public abstract class Visitor
    {
        public abstract void VisitPipeline(Pipeline pipeline);

        public abstract void VisitGroup(Group group);

        public abstract void VisitAction(Action action, int depth);

    }
}
