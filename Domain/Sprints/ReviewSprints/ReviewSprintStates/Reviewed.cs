﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Sprints.ReviewSprints.ReviewSprintStates
{
    public class Reviewed : ReviewSprintStatus
    {
        public Reviewed(ReviewSprint reviewSprint) : base(reviewSprint)
        {
        }

        public override void Start()
        {
            Console.WriteLine("The sprint has already been started.");
        }

        public override void Finish()
        {
            Console.WriteLine("The sprint has already been finished.");
        }

        public override void Review()
        {
            Console.WriteLine("The sprint has already been reviewed.");
        }

        public override void Complete()
        {
            if (ReviewSprint.SummaryDocument != null)
            {
                ReviewSprint.setStatus(new Completed(ReviewSprint));
                Console.WriteLine("The sprint is completed.");
            }
            else {
                Console.WriteLine("Document still has to be added");
            }
            
        }
    }
}
