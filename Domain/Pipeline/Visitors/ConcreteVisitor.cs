﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Pipeline.Visitors
{
    public class ConcreteVisitor : Visitor
    {
        public override void VisitAction(Action action, int depth)
        {
            Console.WriteLine(new String('-', depth) + action.Command);
        }

        public override void VisitGroup(Group group)
        {
            Console.WriteLine("-" + group.Name);
        }

        public override void VisitPipeline(Pipeline pipeline)
        {
            Console.WriteLine("---" + pipeline.Name + "---");
        }
    }
}
