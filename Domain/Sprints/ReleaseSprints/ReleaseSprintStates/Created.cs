﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Sprints.ReleaseSprints.ReleaseSprintStates
{
    public class Created : ReleaseSprintStatus
    {
        public Created(ReleaseSprint releaseSprint) : base(releaseSprint)
        {
        }

        public override void Start()
        {
            ReleaseSprint.setStatus(new InProgress(ReleaseSprint));
            Console.WriteLine("The sprint is started.");
        }

        public override void Finish()
        {
            Console.WriteLine("The sprint has to be started before finishing.");
        }

        public override void Release()
        {
            Console.WriteLine("The sprint has to be started before releasing.");
        }

        public override void Complete()
        {
            Console.WriteLine("The sprint has to be started before completing.");
        }

    }
}
