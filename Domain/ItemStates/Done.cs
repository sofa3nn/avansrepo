﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.ItemStates
{
    public class Done : ItemStatus
    {
        public Done(Item item) : base(item)
        {
        }

        public override void ChooseItem()
        {
            Console.WriteLine("This Item has already been chosen.");
        }

        public override void Finish()
        {
            Console.WriteLine("This Item has already been finished.");
        }

        public override void Test()
        {
            Console.WriteLine("This Item is already in testing.");
        }

        public override void ReDo()
        {
            Item.setStatus(new ToDo(Item));
            Console.WriteLine("This Item must be redone");
        }

        public override void FinishedTesting()
        {
            Console.WriteLine("This Item is already finished testing.");
        }

        public override void Complete()
        {
            Console.WriteLine("This Item is already completed.");
        }
    }
}
