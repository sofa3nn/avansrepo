﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.ActivityStates
{
    //State Pattern
    public abstract class ActivityStatus
    {

        public Activity Activity { get; set; }

        public ActivityStatus(Activity activity)
        {
            Activity = activity;
        }

        public abstract void GiveUp();
        public abstract void PickUp();
        public abstract void Finished();
    }
}
