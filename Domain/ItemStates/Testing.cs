﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.ItemStates
{
    public class Testing : ItemStatus
    {
        public Testing(Item item) : base(item)
        {
        }

        public override void ChooseItem()
        {
            Console.WriteLine("This Item has already been chosen.");
        }

        public override void Finish()
        {
            Console.WriteLine("This Item has already been finished.");
        }

        public override void Test()
        {
            Console.WriteLine("This Item is already in testing.");
        }

        public override void ReDo()
        {
            Item.setStatus(new ToDo(Item));
            Item.publish();
            Console.WriteLine("This Item is back in ToDo.");
        }

        public override void FinishedTesting()
        {
            Item.setStatus(new Tested(Item));
            Console.WriteLine("This Item is finished testing.");
        }

        public override void Complete()
        {
            Console.WriteLine("This Item has to be finished testing first.");
        }
    }
}
