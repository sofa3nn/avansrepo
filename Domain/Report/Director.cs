﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Report
{
    public class Director
    {
        public Builder Builder;

        public void SetBuilder(Builder builder)
        {
            Builder = builder;
        }

        public Report ConstructReportNoFooterNoHeader(string Title, string Content, Export Export) 
        {
            Builder.SetTitle(Title);
            Builder.SetContent(Content);
            Builder.SetExport(Export);

            return Builder.GetResult();
        }
        public Report ConstructReportFooterNoHeader(string Title, string Content, string Footer, Export Export)
        {
            Builder.SetTitle(Title);
            Builder.SetContent(Content);
            Builder.SetFooter(Footer);
            Builder.SetExport(Export);

            return Builder.GetResult();
        }
        public Report ConstructReportNoFooterHeader(string Title, string Content, string Header, Export Export)
        {
            Builder.SetHeader(Header);
            Builder.SetTitle(Title);
            Builder.SetContent(Content);
            Builder.SetExport(Export);

            return Builder.GetResult();
        }
        public Report ConstructReportFooterHeader(string Title, string Content, string Header, string Footer, Export Export)
        {
            Builder.SetHeader(Header);
            Builder.SetTitle(Title);
            Builder.SetContent(Content);
            Builder.SetFooter(Footer);
            Builder.SetExport(Export);

            return Builder.GetResult();
        }
    }
}
