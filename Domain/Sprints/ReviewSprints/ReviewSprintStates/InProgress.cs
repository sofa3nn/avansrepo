﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Sprints.ReviewSprints.ReviewSprintStates
{
    public class InProgress : ReviewSprintStatus
    {
        public InProgress(ReviewSprint reviewSprint) : base(reviewSprint)
        {
        }

        public override void Start()
        {
            Console.WriteLine("The sprint has already been started.");
        }

        public override void Finish()
        {
            ReviewSprint.setStatus(new Finished(ReviewSprint));
            Console.WriteLine("The sprint is finished.");
        }

        public override void Review()
        {
            Console.WriteLine("The sprint has to be finished before releasing.");
        }

        public override void Complete()
        {
            Console.WriteLine("The sprint has to be finished before completing.");
        }

    }
}
