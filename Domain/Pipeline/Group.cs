﻿using Domain.Composites;
using Domain.Pipeline.Visitors;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Pipeline
{
    public class Group : CompositeComponent
    {
        public string Name { get; set; }

        public Group(string name)
        {
            Name = name;
        }

        public override void AcceptVisitor(Object obj, int depth)
        {
            Visitor? visitor = obj as Visitor;
            visitor?.VisitGroup(this);
            base.AcceptVisitor(visitor, depth );
        }
    }
}
