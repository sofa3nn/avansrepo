﻿using Domain;
using Domain.ItemStates;
using Domain.Persons;
using Domain.ActivityStates;
using System.Collections.Generic;
using Xunit;

namespace UnitTests
{
    public class ItemTests
    {
        [Fact]
        public void TestIfItemIsCreatedWithStatusToDo()
        {
            //Arrange
            Item item = new Item(0, "Test item");
            Domain.ItemStates.ToDo Status = new Domain.ItemStates.ToDo(item);

            //Act


            //Assert
            Assert.IsType(Status.GetType(), item.Status);
        }
        [Fact]
        public void TestIfItemIsDoing()
        {
            //Arrange
            Item item = new Item(0, "Test item");
            Domain.ItemStates.Doing Status = new Domain.ItemStates.Doing(item);

            //Act
            item.setStatus(new Domain.ItemStates.Doing(item));

            //Assert
            Assert.IsType(Status.GetType(), item.Status);
        }
        [Fact]
        public void TestIfItemIsReadyForTesting()
        {
            //Arrange
            Item item = new Item(0, "Test item");
            ReadyForTesting Status = new ReadyForTesting(item);

            //Act
            item.setStatus(new ReadyForTesting(item));

            //Assert
            Assert.IsType(Status.GetType(), item.Status);
        }
        [Fact]
        public void TestIfItemIsTesting()
        {
            //Arrange
            Item item = new Item(0, "Test item");
            Testing Status = new Testing(item);

            //Act
            item.setStatus(new Testing(item));

            //Assert
            Assert.IsType(Status.GetType(), item.Status);
        }
        [Fact]
        public void TestIfItemIsTested()
        {
            //Arrange
            Item item = new Item(0, "Test item");
            Tested Status = new Tested(item);

            //Act
            item.setStatus(new Tested(item));

            //Assert
            Assert.IsType(Status.GetType(), item.Status);
        }
        [Fact]
        public void TestIfItemIsDone()
        {
            //Arrange
            Item item = new Item(0, "Test item");
            Domain.ItemStates.Done Status = new Domain.ItemStates.Done(item);

            //Act
            item.setStatus(new Domain.ItemStates.Done(item));

            //Assert
            Assert.IsType(Status.GetType(), item.Status);
        }

        [Fact]
        public void TestIfItemBecomesDoingWhenChooseItemFromToDo()
        {
            //Arrange
            Item item = new Item(0, "Test item");
            Domain.ItemStates.Doing Status = new Domain.ItemStates.Doing(item);
            //Act
            item.Status.ChooseItem();


            //Assert
            Assert.IsType(Status.GetType(), item.Status);
        }
        [Fact]
        public void TestIfItemStaysToDoWhenFinishFromToDo()
        {
            //Arrange
            Item item = new Item(0, "Test item");
            Domain.ItemStates.ToDo Status = new Domain.ItemStates.ToDo(item);
            //Act
            item.Status.Finish();


            //Assert
            Assert.IsType(Status.GetType(), item.Status);
        }
        [Fact]
        public void TestIfItemStaysToDoWhenTestFromToDo()
        {
            //Arrange
            Item item = new Item(0, "Test item");
            Domain.ItemStates.ToDo Status = new Domain.ItemStates.ToDo(item);
            //Act
            item.Status.Test();


            //Assert
            Assert.IsType(Status.GetType(), item.Status);
        }
        [Fact]
        public void TestIfItemStaysToDoWhenFinishedTestingFromToDo()
        {
            //Arrange
            Item item = new Item(0, "Test item");
            Domain.ItemStates.ToDo Status = new Domain.ItemStates.ToDo(item);
            //Act
            item.Status.FinishedTesting();


            //Assert
            Assert.IsType(Status.GetType(), item.Status);
        }
        [Fact]
        public void TestIfItemStaysToDoWhenFinishedCompleteFromToDo()
        {
            //Arrange
            Item item = new Item(0, "Test item");
            Domain.ItemStates.ToDo Status = new Domain.ItemStates.ToDo(item);
            //Act
            item.Status.Complete();


            //Assert
            Assert.IsType(Status.GetType(), item.Status);
        }
        [Fact]
        public void TestIfItemStaysToDoWhenReDoFromToDo()
        {
            //Arrange
            Item item = new Item(0, "Test item");
            Domain.ItemStates.ToDo Status = new Domain.ItemStates.ToDo(item);
            //Act
            item.Status.ReDo();


            //Assert
            Assert.IsType(Status.GetType(), item.Status);
        }

        [Fact]
        public void TestIfItemBecomesReadyForTestingWhenFinishFromDoing()
        {
            //Arrange
            Item item = new Item(0, "Test item");
            ReadyForTesting Status = new ReadyForTesting(item);
            //Act
            item.Status.ChooseItem();
            item.Status.Finish();


            //Assert
            Assert.IsType(Status.GetType(), item.Status);
        }
        [Fact]
        public void TestIfItemStaysDoingWhenChooseItemFromDoing()
        {
            //Arrange
            Item item = new Item(0, "Test item");
            Domain.ItemStates.Doing Status = new Domain.ItemStates.Doing(item);
            //Act
            item.Status.ChooseItem();
            item.Status.ChooseItem();


            //Assert
            Assert.IsType(Status.GetType(), item.Status);
        }
        [Fact]
        public void TestIfItemStaysDoingWhenTestFromDoing()
        {
            //Arrange
            Item item = new Item(0, "Test item");
            Domain.ItemStates.Doing Status = new Domain.ItemStates.Doing(item);
            //Act
            item.Status.ChooseItem();
            item.Status.Test();


            //Assert
            Assert.IsType(Status.GetType(), item.Status);
        }
        [Fact]
        public void TestIfItemStaysDoingWhenReDoFromDoing()
        {
            //Arrange
            Item item = new Item(0, "Test item");
            Domain.ItemStates.Doing Status = new Domain.ItemStates.Doing(item);
            //Act
            item.Status.ChooseItem();
            item.Status.ReDo();


            //Assert
            Assert.IsType(Status.GetType(), item.Status);
        }
        [Fact]
        public void TestIfItemStaysDoingWhenFinishTestingFromDoing()
        {
            //Arrange
            Item item = new Item(0, "Test item");
            Domain.ItemStates.Doing Status = new Domain.ItemStates.Doing(item);
            //Act
            item.Status.ChooseItem();
            item.Status.FinishedTesting();


            //Assert
            Assert.IsType(Status.GetType(), item.Status);
        }
        [Fact]
        public void TestIfItemStaysDoingWhenCompleteFromDoing()
        {
            //Arrange
            Item item = new Item(0, "Test item");
            Domain.ItemStates.Doing Status = new Domain.ItemStates.Doing(item);
            //Act
            item.Status.ChooseItem();
            item.Status.Complete();


            //Assert
            Assert.IsType(Status.GetType(), item.Status);
        }

        [Fact]
        public void TestIfItemBecomesTestingWhenTestFromReadyForTesting()
        {
            //Arrange
            Item item = new Item(0, "Test item");
            Testing Status = new Testing(item);
            //Act
            item.Status.ChooseItem();
            item.Status.Finish();
            item.Status.Test();


            //Assert
            Assert.IsType(Status.GetType(), item.Status);
        }
        [Fact]
        public void TestIfItemStaysReadyForTestingWhenChooseItemFromReadyForTesting()
        {
            //Arrange
            Item item = new Item(0, "Test item");
            ReadyForTesting Status = new ReadyForTesting(item);
            //Act
            item.Status.ChooseItem();
            item.Status.Finish();
            item.Status.ChooseItem();


            //Assert
            Assert.IsType(Status.GetType(), item.Status);
        }
        [Fact]
        public void TestIfItemStaysReadyForTestingWhenFinishFromReadyForTesting()
        {
            //Arrange
            Item item = new Item(0, "Test item");
            ReadyForTesting Status = new ReadyForTesting(item);
            //Act
            item.Status.ChooseItem();
            item.Status.Finish();
            item.Status.Finish();


            //Assert
            Assert.IsType(Status.GetType(), item.Status);
        }
        [Fact]
        public void TestIfItemStaysReadyForTestingWhenReDoFromReadyForTesting()
        {
            //Arrange
            Item item = new Item(0, "Test item");
            ReadyForTesting Status = new ReadyForTesting(item);
            //Act
            item.Status.ChooseItem();
            item.Status.Finish();
            item.Status.ReDo();


            //Assert
            Assert.IsType(Status.GetType(), item.Status);
        }
        [Fact]
        public void TestIfItemStaysReadyForTestingWhenFinishTestingFromReadyForTesting()
        {
            //Arrange
            Item item = new Item(0, "Test item");
            ReadyForTesting Status = new ReadyForTesting(item);
            //Act
            item.Status.ChooseItem();
            item.Status.Finish();
            item.Status.FinishedTesting();


            //Assert
            Assert.IsType(Status.GetType(), item.Status);
        }
        [Fact]
        public void TestIfItemStaysReadyForTestingWhenCompleteFromReadyForTesting()
        {
            //Arrange
            Item item = new Item(0, "Test item");
            ReadyForTesting Status = new ReadyForTesting(item);
            //Act
            item.Status.ChooseItem();
            item.Status.Finish();
            item.Status.Complete();


            //Assert
            Assert.IsType(Status.GetType(), item.Status);
        }

        [Fact]
        public void TestIfItemBecomesTestedWhenFinishTestingFromTesting()
        {
            //Arrange
            Item item = new Item(0, "Test item");
            Tested Status = new Tested(item);
            //Act
            item.Status.ChooseItem();
            item.Status.Finish();
            item.Status.Test();
            item.Status.FinishedTesting();


            //Assert
            Assert.IsType(Status.GetType(), item.Status);
        }
        [Fact]
        public void TestIfItemBecomesToDoWhenReDoFromTesting()
        {
            //Arrange
            Item item = new Item(0, "Test item");
            Domain.ItemStates.ToDo Status = new Domain.ItemStates.ToDo(item);
            //Act
            item.Status.ChooseItem();
            item.Status.Finish();
            item.Status.Test();
            item.Status.ReDo();


            //Assert
            Assert.IsType(Status.GetType(), item.Status);
        }
        [Fact]
        public void TestIfItemStaysTestingWhenChooseItemFromTesting()
        {
            //Arrange
            Item item = new Item(0, "Test item");
            Testing Status = new Testing(item);
            //Act
            item.Status.ChooseItem();
            item.Status.Finish();
            item.Status.Test();
            item.Status.ChooseItem();


            //Assert
            Assert.IsType(Status.GetType(), item.Status);
        }
        [Fact]
        public void TestIfItemStaysTestingWhenFinishFromTesting()
        {
            //Arrange
            Item item = new Item(0, "Test item");
            Testing Status = new Testing(item);
            //Act
            item.Status.ChooseItem();
            item.Status.Finish();
            item.Status.Test();
            item.Status.Finish();


            //Assert
            Assert.IsType(Status.GetType(), item.Status);
        }
        [Fact]
        public void TestIfItemStaysTestingWhenTestFromTesting()
        {
            //Arrange
            Item item = new Item(0, "Test item");
            Testing Status = new Testing(item);
            //Act
            item.Status.ChooseItem();
            item.Status.Finish();
            item.Status.Test();
            item.Status.Test();


            //Assert
            Assert.IsType(Status.GetType(), item.Status);
        }
        [Fact]
        public void TestIfItemStaysTestingWhenCompleteFromTesting()
        {
            //Arrange
            Item item = new Item(0, "Test item");
            Testing Status = new Testing(item);
            //Act
            item.Status.ChooseItem();
            item.Status.Finish();
            item.Status.Test();
            item.Status.Complete();


            //Assert
            Assert.IsType(Status.GetType(), item.Status);
        }

        [Fact]
        public void TestIfItemBecomesDoneWhenCompleteFromTested()
        {
            //Arrange
            Item item = new Item(0, "Test item");
            Domain.ItemStates.Done Status = new Domain.ItemStates.Done(item);
            //Act
            item.Status.ChooseItem();
            item.Status.Finish();
            item.Status.Test();
            item.Status.FinishedTesting();
            item.Status.Complete();


            //Assert
            Assert.IsType(Status.GetType(), item.Status);
        }
        [Fact]
        public void TestIfItemBecomesReadyForTestingWhenFinishFromTested()
        {
            //Arrange
            Item item = new Item(0, "Test item");
            ReadyForTesting Status = new ReadyForTesting(item);
            //Act
            item.Status.ChooseItem();
            item.Status.Finish();
            item.Status.Test();
            item.Status.FinishedTesting();
            item.Status.Finish();


            //Assert
            Assert.IsType(Status.GetType(), item.Status);
        }
        [Fact]
        public void TestIfItemStaysTestedWhenChooseItemFromTested()
        {
            //Arrange
            Item item = new Item(0, "Test item");
            Tested Status = new Tested(item);
            //Act
            item.Status.ChooseItem();
            item.Status.Finish();
            item.Status.Test();
            item.Status.FinishedTesting();
            item.Status.ChooseItem();


            //Assert
            Assert.IsType(Status.GetType(), item.Status);
        }
        [Fact]
        public void TestIfItemStaysTestedWhenTestFromTested()
        {
            //Arrange
            Item item = new Item(0, "Test item");
            Tested Status = new Tested(item);
            //Act
            item.Status.ChooseItem();
            item.Status.Finish();
            item.Status.Test();
            item.Status.FinishedTesting();
            item.Status.Test();


            //Assert
            Assert.IsType(Status.GetType(), item.Status);
        }
        [Fact]
        public void TestIfItemStaysTestedWhenReDoFromTested()
        {
            //Arrange
            Item item = new Item(0, "Test item");
            Tested Status = new Tested(item);
            //Act
            item.Status.ChooseItem();
            item.Status.Finish();
            item.Status.Test();
            item.Status.FinishedTesting();
            item.Status.ReDo();


            //Assert
            Assert.IsType(Status.GetType(), item.Status);
        }
        [Fact]
        public void TestIfItemStaysTestedWhenFinishTestingFromTested()
        {
            //Arrange
            Item item = new Item(0, "Test item");
            Tested Status = new Tested(item);
            //Act
            item.Status.ChooseItem();
            item.Status.Finish();
            item.Status.Test();
            item.Status.FinishedTesting();
            item.Status.FinishedTesting();


            //Assert
            Assert.IsType(Status.GetType(), item.Status);
        }

        [Fact]
        public void TestIfItemStaysDoneWhenChooseItemFromDone()
        {
            //Arrange
            Item item = new Item(0, "Test item");
            Domain.ItemStates.Done Status = new Domain.ItemStates.Done(item);
            //Act
            item.Status.ChooseItem();
            item.Status.Finish();
            item.Status.Test();
            item.Status.FinishedTesting();
            item.Status.Complete();
            item.Status.ChooseItem();


            //Assert
            Assert.IsType(Status.GetType(), item.Status);
        }
        [Fact]
        public void TestIfItemStaysDoneWhenFinishFromDone()
        {
            //Arrange
            Item item = new Item(0, "Test item");
            Domain.ItemStates.Done Status = new Domain.ItemStates.Done(item);
            //Act
            item.Status.ChooseItem();
            item.Status.Finish();
            item.Status.Test();
            item.Status.FinishedTesting();
            item.Status.Complete();
            item.Status.Finish();


            //Assert
            Assert.IsType(Status.GetType(), item.Status);
        }
        [Fact]
        public void TestIfItemStaysDoneWhenTestFromDone()
        {
            //Arrange
            Item item = new Item(0, "Test item");
            Domain.ItemStates.Done Status = new Domain.ItemStates.Done(item);
            //Act
            item.Status.ChooseItem();
            item.Status.Finish();
            item.Status.Test();
            item.Status.FinishedTesting();
            item.Status.Complete();
            item.Status.Test();


            //Assert
            Assert.IsType(Status.GetType(), item.Status);
        }
        [Fact]
        public void TestIfItemBecomesToDoWhenReDoFromDone()
        {
            //Arrange
            Item item = new Item(0, "Test item");
            Domain.ItemStates.ToDo Status = new Domain.ItemStates.ToDo(item);
            //Act
            item.Status.ChooseItem();
            item.Status.Finish();
            item.Status.Test();
            item.Status.FinishedTesting();
            item.Status.Complete();
            item.Status.ReDo();


            //Assert
            Assert.IsType(Status.GetType(), item.Status);
        }
        [Fact]
        public void TestIfItemStaysDoneWhenFinishTestingFromDone()
        {
            //Arrange
            Item item = new Item(0, "Test item");
            Domain.ItemStates.Done Status = new Domain.ItemStates.Done(item);
            //Act
            item.Status.ChooseItem();
            item.Status.Finish();
            item.Status.Test();
            item.Status.FinishedTesting();
            item.Status.Complete();
            item.Status.FinishedTesting();


            //Assert
            Assert.IsType(Status.GetType(), item.Status);
        }
        [Fact]
        public void TestIfItemStaysDoneWhenCompleteFromDone()
        {
            //Arrange
            Item item = new Item(0, "Test item");
            Domain.ItemStates.Done Status = new Domain.ItemStates.Done(item);
            //Act
            item.Status.ChooseItem();
            item.Status.Finish();
            item.Status.Test();
            item.Status.FinishedTesting();
            item.Status.Complete();
            item.Status.Complete();


            //Assert
            Assert.IsType(Status.GetType(), item.Status);
        }
        [Fact]
        public void TestIfActivitiesCanBeAddedToItem()
        {
            //Arrange
            Item item = new Item(0, "Test item");
            Activity activity = new Activity(1, "Test activity");
            Activity activity2 = new Activity(2, "Test activity2");
            //Act
            item.AddActivity(activity);
            item.AddActivity(activity2);
            //Assert
            Assert.Equal(activity, item.Activities[0]);
            Assert.Equal(activity2, item.Activities[1]);
        }
        [Fact]
        public void TestIfItemCanExistWithoutActivities()
        {
            //Arrange
            Item item = new Item(0, "Test item");
            List<Activity> list = new List<Activity>();
            //Act

            //Assert
            Assert.Equal(list, item.Activities);

        }
        [Fact]
        public void TestIfItemCanBePickedUpByOneDeveloper()
        {
            //Arrange
            Item item = new Item(0, "Test item");
            Developer Developer = new Developer(1, "Name", "Email", "telePhoneNumber");
            Developer Developer2 = new Developer(1, "Name2", "Email2", "telePhoneNumber2");
            //Act
            item.PickupItem(Developer);
            item.PickupItem(Developer2);
            //Assert
            Assert.Equal(Developer2, item.Developer);
        }
        [Fact]
        public void TestIfItemCanExistWithoutDeveloper()
        {
            //Arrange
            Item item = new Item(0, "Test item");

            //Act

            //Assert
            Assert.Null(item.Developer);

        }
        [Fact]
        public void TestIfItemCantBeDoneWhenAllActivitiesAreNotDone() 
        {
            Item item = new Item(0, "Test item");
            Tested tested = new Tested(item);
            Domain.ItemStates.Done done = new Domain.ItemStates.Done(item);
            Activity activity = new Activity(0, "Test");
            Activity activity1 = new Activity(1, "Test");
            activity1.setStatus(new Domain.ActivityStates.Doing(activity1));
            item.AddActivity(activity);
            item.AddActivity(activity1);
            item.setStatus(new Tested(item));
            item.Status.Complete();
            Assert.IsNotType(item.Status.GetType(), done);
            Assert.IsType(item.Status.GetType(), tested);
        }
        [Fact]
        public void TestIfItemIsDoneWhenAllActivitiesAreDone()
        {
            Item item = new Item(0, "Test item");
            Tested tested = new Tested(item);
            Domain.ItemStates.Done done = new Domain.ItemStates.Done(item);
            Activity activity = new Activity(0, "Test");
            Activity activity1 = new Activity(1, "Test");
            activity.setStatus(new Domain.ActivityStates.Done(activity));
            activity1.setStatus(new Domain.ActivityStates.Done(activity1));
            item.AddActivity(activity);
            item.AddActivity(activity1);
            item.setStatus(new Tested(item));
            item.Status.Complete();
            Assert.IsNotType(item.Status.GetType(), tested);
            Assert.IsType(item.Status.GetType(), done);
        }
    }
}
