﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Persons
{
    public class ScrumMaster : Person
    {
        public ScrumMaster(int id, string name, string email, string telephoneNumber) : base(id, name, email, telephoneNumber)
        {
        }
    }
}
