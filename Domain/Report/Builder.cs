﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Report
{
    public interface Builder
    {
        public void SetFooter(string footer);
        public void SetHeader(string header);
        public void SetTitle(string title);
        public void SetContent(string content);
        public void SetExport(Export export);
        public Report GetResult();
    }
}
