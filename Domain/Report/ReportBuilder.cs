﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Report
{
    public class ReportBuilder : Builder
    {
        public Report report = new Report();

        public Report GetResult()
        {
            return report;
        }

        public void SetContent(string content)
        {
            report.Content = content;
        }

        public void SetExport(Export export)
        {
            report.Export = export;
        }

        public void SetFooter(string footer)
        {
            report.Footer = footer;
        }

        public void SetHeader(string header)
        {
            report.Header = header;
        }

        public void SetTitle(string title)
        {
            report.Title = title;
        }
    }
}
