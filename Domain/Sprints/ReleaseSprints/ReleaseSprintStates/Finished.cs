﻿using Domain.Pipeline.Visitors;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Sprints.ReleaseSprints.ReleaseSprintStates
{
    public class Finished : ReleaseSprintStatus
    {
        public Finished(ReleaseSprint releaseSprint) : base(releaseSprint)
        {
        }

        public override void Start()
        {
            Console.WriteLine("The sprint has already been started.");
        }

        public override void Finish()
        {
            Console.WriteLine("The sprint has already been finished.");
        }

        public override void Release()
        {
            ReleaseSprint.setStatus(new Released(ReleaseSprint));
            ReleaseSprint.StartPipeline(new ConcreteVisitor(), 0);
            Console.WriteLine("The sprint is released.");
        }

        public override void Complete()
        {
            Console.WriteLine("The sprint has to be released before completing.");
        }
    }
}
