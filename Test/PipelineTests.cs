﻿using Domain.Pipeline;
using Domain.Pipeline.Visitors;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit.Abstractions;
using Xunit;
using Domain.Sprints.ReleaseSprints;
using Domain.Sprints.ReleaseSprints.ReleaseSprintStates;

[assembly: CollectionBehavior(DisableTestParallelization = true)] 
namespace UnitTests
{
    public class PipelineTests
    {
       
        [Fact]
        public async void TestIfPipelineCanBeConfiguredAndStarted()
        {   
             StringWriter sw = new StringWriter();
             Console.SetOut(sw);

            //Arrange
             ReleaseSprint sprint = new ReleaseSprint(0,"",new DateTime(),new DateTime());
             sprint.setStatus(new Finished(sprint));
             Pipeline pipeline = new Pipeline("TestPipeline");
             sprint.Pipeline = pipeline;
             Group group = new Group("Test");
             sprint.Pipeline.AddComponent(group);

             group.AddComponent(new Domain.Pipeline.Action("TestCode"));
             group.AddComponent(new Domain.Pipeline.Action("TestCode2"));

             ConcreteVisitor concreteVisitor = new ConcreteVisitor();
             StringBuilder sb = sw.GetStringBuilder();
             sb.Remove(0, sb.Length);
             //Act
             sprint.StartPipeline((object)concreteVisitor, 1);

             //Assert
             Assert.Equal("The pipeline has started\n---TestPipeline---\n-Test\n-----TestCode\n-----TestCode2\n", sw.ToString());
             StreamWriter standardOutput = new StreamWriter(Console.OpenStandardOutput());
             standardOutput.AutoFlush = true;
             Console.SetOut(standardOutput);
            
             
        }
       
    }
}
