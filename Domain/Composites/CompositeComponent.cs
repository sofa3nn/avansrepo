﻿using Domain.Pipeline.Visitors;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Composites
{
    public class CompositeComponent : Component
    {
        private List<Component> parts = new List<Component>();

        public CompositeComponent()
        {
            
        }

        public void AddComponent(Component component)
        {
            parts.Add(component);
        }
        public void RemoveComponent(Component component)
        {
            parts.Remove(component);
        }
        public Component GetComponent(int i)
        {
            return parts[i];
        }

        public int GetSize()
        {
            return parts.Count;
        }

        public override void AcceptVisitor(Object visitor, int depth)
        {
            foreach (Component component in parts)
            {
                component.AcceptVisitor(visitor, depth+2);
            }
        }
    }
}
