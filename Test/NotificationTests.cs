﻿using Domain;
using Domain.Comments;
using Domain.Comments.Visitors;
using Domain.ItemStates;
using Domain.Observer;
using Domain.Observer.Subscribers;
using Domain.Pipeline;
using Domain.Sprints.ReleaseSprints;
using Domain.Sprints.ReleaseSprints.ReleaseSprintStates;
using Domain.Sprints.ReviewSprints;
using Domain.Sprints.ReviewSprints.ReviewSprintStates;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace UnitTests
{
    public class NotificationTests
    {
        [Fact]
        public void TestIfEmailIsSendWhenItemBecomesReadyForTesting() {
            StringWriter sw = new StringWriter();
            Console.SetOut(sw);
            //Arrange
            Item item = new Item(0, "Test item");
            Subscriber subscriber = new EmailSubscriber("TestEmail");
            //Act
            item.subscribe(subscriber);
            item.setStatus(new Doing(item));
            StringBuilder sb = sw.GetStringBuilder();
            sb.Remove(0, sb.Length);
            item.Status.Finish();
            //Assert
            Assert.Equal("Item:Domain.ItemStates.ReadyForTesting\nEmail send\nThis Item is ReadyForTesting.\n", sw.ToString());

            StreamWriter standardOutput = new StreamWriter(Console.OpenStandardOutput());
            standardOutput.AutoFlush = true;
            Console.SetOut(standardOutput);
        }
        [Fact]
        public void TestIfSMSIsSendWhenItemBecomesReadyForTesting()
        {
            StringWriter sw = new StringWriter();
            Console.SetOut(sw);
            //Arrange
            Item item = new Item(0, "Test item");
            Subscriber subscriber = new SmsSubscriber("TestSMS");
            //Act
            item.subscribe(subscriber);
            item.setStatus(new Doing(item));
            StringBuilder sb = sw.GetStringBuilder();
            sb.Remove(0, sb.Length);
            item.Status.Finish();
            //Assert
            Assert.Equal("Item:Domain.ItemStates.ReadyForTesting\nSMS send\nThis Item is ReadyForTesting.\n", sw.ToString());

            StreamWriter standardOutput = new StreamWriter(Console.OpenStandardOutput());
            standardOutput.AutoFlush = true;
            Console.SetOut(standardOutput);
        }
        [Fact]
        public void TestIfSlackIsSendWhenItemBecomesReadyForTesting()
        {
            StringWriter sw = new StringWriter();
            Console.SetOut(sw);
            //Arrange
            Item item = new Item(0, "Test item");
            Subscriber subscriber = new SlackSubscriber("TestSlack");
            //Act
            item.subscribe(subscriber);
            item.setStatus(new Doing(item));
            StringBuilder sb = sw.GetStringBuilder();
            sb.Remove(0, sb.Length);
            item.Status.Finish();
            //Assert
            Assert.Equal("Item:Domain.ItemStates.ReadyForTesting\nSlack send\nThis Item is ReadyForTesting.\n", sw.ToString());

            StreamWriter standardOutput = new StreamWriter(Console.OpenStandardOutput());
            standardOutput.AutoFlush = true;
            Console.SetOut(standardOutput);
        }
        
        [Fact]
        public void TestIfEmailIsSendWhenItemBecomesToDoFromTesting()
        {
            StringWriter sw = new StringWriter();
            Console.SetOut(sw);
            //Arrange
            Item item = new Item(0, "Test item");
            Subscriber subscriber = new EmailSubscriber("TestEmail");
            //Act
            item.subscribe(subscriber);
            item.setStatus(new Testing(item));
            StringBuilder sb = sw.GetStringBuilder();
            sb.Remove(0, sb.Length);
            item.Status.ReDo();
            //Assert
            Assert.Equal("Item:Domain.ItemStates.ToDo\nEmail send\nThis Item is back in ToDo.\n", sw.ToString());

            StreamWriter standardOutput = new StreamWriter(Console.OpenStandardOutput());
            standardOutput.AutoFlush = true;
            Console.SetOut(standardOutput);
        }
        [Fact]
        public void TestIfSlackIsSendWhenItemBecomesToDoFromTesting()
        {
            StringWriter sw = new StringWriter();
            Console.SetOut(sw);
            //Arrange
            Item item = new Item(0, "Test item");
            Subscriber subscriber = new SlackSubscriber("TestEmail");
            //Act
            item.subscribe(subscriber);
            item.setStatus(new Testing(item));
            StringBuilder sb = sw.GetStringBuilder();
            sb.Remove(0, sb.Length);
            item.Status.ReDo();
            //Assert
            Assert.Equal("Item:Domain.ItemStates.ToDo\nSlack send\nThis Item is back in ToDo.\n", sw.ToString());

            StreamWriter standardOutput = new StreamWriter(Console.OpenStandardOutput());
            standardOutput.AutoFlush = true;
            Console.SetOut(standardOutput);
        }
        [Fact]
        public void TestIfSmsIsSendWhenItemBecomesToDoFromTesting()
        {
            StringWriter sw = new StringWriter();
            Console.SetOut(sw);
            //Arrange
            Item item = new Item(0, "Test item");
            Subscriber subscriber = new SmsSubscriber("TestEmail");
            //Act
            item.subscribe(subscriber);
            item.setStatus(new Testing(item));
            StringBuilder sb = sw.GetStringBuilder();
            sb.Remove(0, sb.Length);
            item.Status.ReDo();
            //Assert
            Assert.Equal("Item:Domain.ItemStates.ToDo\nSMS send\nThis Item is back in ToDo.\n", sw.ToString());

            StreamWriter standardOutput = new StreamWriter(Console.OpenStandardOutput());
            standardOutput.AutoFlush = true;
            Console.SetOut(standardOutput);
        }
        
        [Fact]
        public void TestIfEmailIsSendWhenReactionOnComment() 
        {
            StringWriter sw = new StringWriter();
            Console.SetOut(sw);
            //Arrange
            Item item = new Item(0, "Test item");
            Subscriber subscriber = new EmailSubscriber("TestSlack");
            ConcreteVisitor concretVisitor = new ConcreteVisitor();
            Comment comment = new Comment("Test");
            Comment comment1 = new Comment("Test2");
            //Act
            item.subscribe(subscriber);
            item.AddComment(comment, null);
            StringBuilder sb = sw.GetStringBuilder();
            sb.Remove(0, sb.Length);
            item.AddComment(comment1, comment);
            //Assert
            Assert.Equal("New message added to Item\nEmail send\n", sw.ToString());
            StreamWriter standardOutput = new StreamWriter(Console.OpenStandardOutput());
            standardOutput.AutoFlush = true;
            Console.SetOut(standardOutput);
        }
        [Fact]
        public void TestIfSlackIsSendWhenReactionOnComment()
        {
            StringWriter sw = new StringWriter();
            Console.SetOut(sw);
            //Arrange
            Item item = new Item(0, "Test item");
            Subscriber subscriber = new SlackSubscriber("TestSlack");
            ConcreteVisitor concretVisitor = new ConcreteVisitor();
            Comment comment = new Comment("Test");
            Comment comment1 = new Comment("Test2");
            //Act
            item.subscribe(subscriber);
            item.AddComment(comment, null);
            StringBuilder sb = sw.GetStringBuilder();
            sb.Remove(0, sb.Length);
            item.AddComment(comment1, comment);
            //Assert
            Assert.Equal("New message added to Item\nSlack send\n", sw.ToString());
            StreamWriter standardOutput = new StreamWriter(Console.OpenStandardOutput());
            standardOutput.AutoFlush = true;
            Console.SetOut(standardOutput);
        }
        [Fact]
        public void TestIfSmsIsSendWhenReactionOnComment()
        {
            StringWriter sw = new StringWriter();
            Console.SetOut(sw);
            //Arrange
            Item item = new Item(0, "Test item");
            Subscriber subscriber = new SmsSubscriber("TestSlack");
            ConcreteVisitor concretVisitor = new ConcreteVisitor();
            Comment comment = new Comment("Test");
            Comment comment1 = new Comment("Test2");
            //Act
            item.subscribe(subscriber);
            item.AddComment(comment, null);
            StringBuilder sb = sw.GetStringBuilder();
            sb.Remove(0, sb.Length);
            item.AddComment(comment1, comment);
            //Assert
            Assert.Equal("New message added to Item\nSMS send\n", sw.ToString());
            StreamWriter standardOutput = new StreamWriter(Console.OpenStandardOutput());
            standardOutput.AutoFlush = true;
            Console.SetOut(standardOutput);
        }

        [Fact]
        public void TestIfEmailIsSendWhenPipelineSuccesfull()
        {
            StringWriter sw = new StringWriter();
            Console.SetOut(sw);
            //Arrange
            Pipeline pipeline = new Pipeline("Test");
            Subscriber subscriber = new EmailSubscriber("TestEmail");
            ConcreteVisitor concretVisitor = new ConcreteVisitor();
            //Act
            pipeline.subscribe(subscriber);
            StringBuilder sb = sw.GetStringBuilder();
            sb.Remove(0, sb.Length);
            pipeline.Complete();
            
            //Assert
            Assert.Equal("The pipeline is complete\nName Pipeline:Test\nEmail send\n", sw.ToString());
            StreamWriter standardOutput = new StreamWriter(Console.OpenStandardOutput());
            standardOutput.AutoFlush = true;
            Console.SetOut(standardOutput);
        }
        [Fact]
        public void TestIfSlackIsSendWhenPipelineSuccesfull()
        {
            StringWriter sw = new StringWriter();
            Console.SetOut(sw);
            //Arrange
            Pipeline pipeline = new Pipeline("Test");
            Subscriber subscriber = new SlackSubscriber("TestSlack");
            ConcreteVisitor concretVisitor = new ConcreteVisitor();
            //Act
            pipeline.subscribe(subscriber);
            StringBuilder sb = sw.GetStringBuilder();
            sb.Remove(0, sb.Length);
            pipeline.Complete();

            //Assert
            Assert.Equal("The pipeline is complete\nName Pipeline:Test\nSlack send\n", sw.ToString());
            StreamWriter standardOutput = new StreamWriter(Console.OpenStandardOutput());
            standardOutput.AutoFlush = true;
            Console.SetOut(standardOutput);
        }
        [Fact]
        public void TestIfSmsIsSendWhenPipelineSuccesfull()
        {
            StringWriter sw = new StringWriter();
            Console.SetOut(sw);
            //Arrange
            Pipeline pipeline = new Pipeline("Test");
            Subscriber subscriber = new SmsSubscriber("TestSms");
            ConcreteVisitor concretVisitor = new ConcreteVisitor();
            //Act
            pipeline.subscribe(subscriber);
            StringBuilder sb = sw.GetStringBuilder();
            sb.Remove(0, sb.Length);
            pipeline.Complete();

            //Assert
            Assert.Equal("The pipeline is complete\nName Pipeline:Test\nSMS send\n", sw.ToString());
            StreamWriter standardOutput = new StreamWriter(Console.OpenStandardOutput());
            standardOutput.AutoFlush = true;
            Console.SetOut(standardOutput);
        }
        [Fact]
        public void TestIfEmailIsSendWhenPipelineCanceled()
        {
            StringWriter sw = new StringWriter();
            Console.SetOut(sw);
            //Arrange
            Pipeline pipeline = new Pipeline("Test");
            Subscriber subscriber = new EmailSubscriber("TestEmail");
            ConcreteVisitor concretVisitor = new ConcreteVisitor();
            //Act
            pipeline.subscribe(subscriber);
            StringBuilder sb = sw.GetStringBuilder();
            sb.Remove(0, sb.Length);
            pipeline.Canceled();

            //Assert
            Assert.Equal("The pipeline has been Canceled\nName Pipeline:Test\nEmail send\n", sw.ToString());
            StreamWriter standardOutput = new StreamWriter(Console.OpenStandardOutput());
            standardOutput.AutoFlush = true;
            Console.SetOut(standardOutput);
        }
        [Fact]
        public void TestIfSlackIsSendWhenPipelineCanceled()
        {
            StringWriter sw = new StringWriter();
            Console.SetOut(sw);
            //Arrange
            Pipeline pipeline = new Pipeline("Test");
            Subscriber subscriber = new SlackSubscriber("TestSlack");
            ConcreteVisitor concretVisitor = new ConcreteVisitor();
            //Act
            pipeline.subscribe(subscriber);
            StringBuilder sb = sw.GetStringBuilder();
            sb.Remove(0, sb.Length);
            pipeline.Canceled();

            //Assert
            Assert.Equal("The pipeline has been Canceled\nName Pipeline:Test\nSlack send\n", sw.ToString());
            StreamWriter standardOutput = new StreamWriter(Console.OpenStandardOutput());
            standardOutput.AutoFlush = true;
            Console.SetOut(standardOutput);
        }
        [Fact]
        public void TestIfSmsIsSendWhenPipelineCanceled()
        {
            StringWriter sw = new StringWriter();
            Console.SetOut(sw);
            //Arrange
            Pipeline pipeline = new Pipeline("Test");
            Subscriber subscriber = new SmsSubscriber("TestSms");
            ConcreteVisitor concretVisitor = new ConcreteVisitor();
            //Act
            pipeline.subscribe(subscriber);
            StringBuilder sb = sw.GetStringBuilder();
            sb.Remove(0, sb.Length);
            pipeline.Canceled();

            //Assert
            Assert.Equal("The pipeline has been Canceled\nName Pipeline:Test\nSMS send\n", sw.ToString());
            StreamWriter standardOutput = new StreamWriter(Console.OpenStandardOutput());
            standardOutput.AutoFlush = true;
            Console.SetOut(standardOutput);
        }
        [Fact]
        public void TestIfEmailIsSendWhenStateOfReleaseSprintIsChanged()
        {
            StringWriter sw = new StringWriter();
            Console.SetOut(sw);
            //Arrange
            ReleaseSprint sprint = new ReleaseSprint(1,"Test", new DateTime(),new DateTime());
            Subscriber subscriber = new EmailSubscriber("TestEmail");
            
            //Act
            sprint.subscribe(subscriber);
            StringBuilder sb = sw.GetStringBuilder();
            sb.Remove(0, sb.Length);
            sprint.setStatus(new Domain.Sprints.ReleaseSprints.ReleaseSprintStates.Completed(sprint));

            //Assert
            Assert.Equal("Status:Domain.Sprints.ReleaseSprints.ReleaseSprintStates.Completed\nEmail send\n", sw.ToString());
            StreamWriter standardOutput = new StreamWriter(Console.OpenStandardOutput());
            standardOutput.AutoFlush = true;
            Console.SetOut(standardOutput);
        }
        [Fact]
        public void TestIfSlackIsSendWhenStateOfReleaseSprintIsChanged()
        {
            StringWriter sw = new StringWriter();
            Console.SetOut(sw);
            //Arrange
            ReleaseSprint sprint = new ReleaseSprint(1, "Test", new DateTime(), new DateTime());
            Subscriber subscriber = new SlackSubscriber("TestEmail");

            //Act
            sprint.subscribe(subscriber);
            StringBuilder sb = sw.GetStringBuilder();
            sb.Remove(0, sb.Length);
            sprint.setStatus(new Domain.Sprints.ReleaseSprints.ReleaseSprintStates.Completed(sprint));

            //Assert
            Assert.Equal("Status:Domain.Sprints.ReleaseSprints.ReleaseSprintStates.Completed\nSlack send\n", sw.ToString());
            StreamWriter standardOutput = new StreamWriter(Console.OpenStandardOutput());
            standardOutput.AutoFlush = true;
            Console.SetOut(standardOutput);
        }
        [Fact]
        public void TestIfSmsIsSendWhenStateOfReleaseSprintIsChanged()
        {
            StringWriter sw = new StringWriter();
            Console.SetOut(sw);
            //Arrange
            ReleaseSprint sprint = new ReleaseSprint(1, "Test", new DateTime(), new DateTime());
            Subscriber subscriber = new SmsSubscriber("TestSms");

            //Act
            sprint.subscribe(subscriber);
            StringBuilder sb = sw.GetStringBuilder();
            sb.Remove(0, sb.Length);
            sprint.setStatus(new Domain.Sprints.ReleaseSprints.ReleaseSprintStates.Completed(sprint));

            //Assert
            Assert.Equal("Status:Domain.Sprints.ReleaseSprints.ReleaseSprintStates.Completed\nSMS send\n", sw.ToString());
            StreamWriter standardOutput = new StreamWriter(Console.OpenStandardOutput());
            standardOutput.AutoFlush = true;
            Console.SetOut(standardOutput);
        }
        [Fact]
        public void TestIfEmailIsSendWhenStateOfReviewSprintIsChanged()
        {
            StringWriter sw = new StringWriter();
            Console.SetOut(sw);
            //Arrange
            ReviewSprint sprint = new ReviewSprint(1, "Test", new DateTime(), new DateTime());
            Subscriber subscriber = new EmailSubscriber("TestEmail");

            //Act
            sprint.subscribe(subscriber);
            StringBuilder sb = sw.GetStringBuilder();
            sb.Remove(0, sb.Length);
            sprint.setStatus(new Domain.Sprints.ReviewSprints.ReviewSprintStates.Completed(sprint));

            //Assert
            Assert.Equal("Status:Domain.Sprints.ReviewSprints.ReviewSprintStates.Completed\nEmail send\n", sw.ToString());
            StreamWriter standardOutput = new StreamWriter(Console.OpenStandardOutput());
            standardOutput.AutoFlush = true;
            Console.SetOut(standardOutput);
        }
        [Fact]
        public void TestIfSlackIsSendWhenStateOfReviewSprintIsChanged()
        {
            StringWriter sw = new StringWriter();
            Console.SetOut(sw);
            //Arrange
            ReviewSprint sprint = new ReviewSprint(1, "Test", new DateTime(), new DateTime());
            Subscriber subscriber = new SlackSubscriber("TestSlack");

            //Act
            sprint.subscribe(subscriber);
            StringBuilder sb = sw.GetStringBuilder();
            sb.Remove(0, sb.Length);
            sprint.setStatus(new Domain.Sprints.ReviewSprints.ReviewSprintStates.Completed(sprint));

            //Assert
            Assert.Equal("Status:Domain.Sprints.ReviewSprints.ReviewSprintStates.Completed\nSlack send\n", sw.ToString());
            StreamWriter standardOutput = new StreamWriter(Console.OpenStandardOutput());
            standardOutput.AutoFlush = true;
            Console.SetOut(standardOutput);
        }
        [Fact]
        public void TestIfSmsIsSendWhenStateOfReviewSprintIsChanged()
        {
            StringWriter sw = new StringWriter();
            Console.SetOut(sw);
            //Arrange
            ReviewSprint sprint = new ReviewSprint(1, "Test", new DateTime(), new DateTime());
            Subscriber subscriber = new SmsSubscriber("TestSms");

            //Act
            sprint.subscribe(subscriber);
            StringBuilder sb = sw.GetStringBuilder();
            sb.Remove(0, sb.Length);
            sprint.setStatus(new Domain.Sprints.ReviewSprints.ReviewSprintStates.Completed(sprint));

            //Assert
            Assert.Equal("Status:Domain.Sprints.ReviewSprints.ReviewSprintStates.Completed\nSMS send\n", sw.ToString());
            StreamWriter standardOutput = new StreamWriter(Console.OpenStandardOutput());
            standardOutput.AutoFlush = true;
            Console.SetOut(standardOutput);
        }
    }
}
