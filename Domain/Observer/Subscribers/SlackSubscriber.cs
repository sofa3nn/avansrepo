﻿using Domain.ItemStates;
using Domain.Sprints.ReleaseSprints.ReleaseSprintStates;
using Domain.Sprints.ReviewSprints.ReviewSprintStates;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Observer.Subscribers
{
    public class SlackSubscriber : Subscriber
    {
        public string SlackUsername;
        public SlackSubscriber(string slackUsername)
        {
            SlackUsername = slackUsername;
        }
        public void updateItem(ItemStatus Status, bool NewMessage)
        {
            if (NewMessage)
            {
                Console.WriteLine("New message added to Item");
                Console.WriteLine("Slack send");
            }
            else 
            { 
                Console.WriteLine("Item:" + Status.ToString());
                Console.WriteLine("Slack send");
            }
            
        }

        public void updatePipeline(string Name)
        {
            Console.WriteLine("Name Pipeline:"+ Name);
            Console.WriteLine("Slack send");
        }

        public void updateReleaseSprint(ReleaseSprintStatus Status)
        {
            Console.WriteLine("Status:" + Status.ToString());
            Console.WriteLine("Slack send");
        }

        public void updateReviewSprint(ReviewSprintStatus Status)
        {
            Console.WriteLine("Status:" + Status.ToString());
            Console.WriteLine("Slack send");
        }
    }
}
