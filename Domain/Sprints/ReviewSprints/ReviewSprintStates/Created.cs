﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Sprints.ReviewSprints.ReviewSprintStates
{
    public class Created : ReviewSprintStatus
    {
        public Created(ReviewSprint reviewSprint) : base(reviewSprint)
        {
        }

        public override void Start()
        {
            ReviewSprint.setStatus(new InProgress(ReviewSprint));
            Console.WriteLine("The sprint is started.");
        }

        public override void Finish()
        {
            Console.WriteLine("The sprint has to be started before finishing.");
        }

        public override void Review()
        {
            Console.WriteLine("The sprint has to be started before releasing.");
        }

        public override void Complete()
        {
            Console.WriteLine("The sprint has to be started before completing.");
        }

    }
}
