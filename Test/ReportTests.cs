﻿using Domain;
using Domain.Report;
using Domain.Sprints.ReleaseSprints;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace UnitTests
{
    public class ReportTests
    {
        [Fact]
        public void TestIfReportCanBeCreatedWithoutAHeaderAndAFooter() 
        {
            StringWriter sw = new StringWriter();
            Console.SetOut(sw);
            //Arrange
            Sprint sprint = new ReleaseSprint(1, "Test", new DateTime(), new DateTime());
            Report report;
            Director director = new Director();
            ReportBuilder reportBuilder = new ReportBuilder();
            Export export = new ExportPDF();
            //Act
            sprint.Director = director;
            director.SetBuilder(reportBuilder);
            report = director.ConstructReportNoFooterNoHeader("Title1", "Content1", export);
            StringBuilder sb = sw.GetStringBuilder();
            sb.Remove(0, sb.Length);
            report.DisplayReport();
            //Assert
            Assert.Equal("Title :Title1\nHeader :\nContent :Content1\nFooter :\n", sw.ToString());
            StreamWriter standardOutput = new StreamWriter(Console.OpenStandardOutput());
            standardOutput.AutoFlush = true;
            Console.SetOut(standardOutput);
        }
        [Fact]
        public void TestIfReportCanBeCreatedWithoutAHeader()
        {
            StringWriter sw = new StringWriter();
            Console.SetOut(sw);
            //Arrange
            Sprint sprint = new ReleaseSprint(1, "Test", new DateTime(), new DateTime());
            Report report;
            Director director = new Director();
            ReportBuilder reportBuilder = new ReportBuilder();
            Export export = new ExportPDF();
            //Act
            sprint.Director = director;
            director.SetBuilder(reportBuilder);
            report = director.ConstructReportFooterNoHeader("Title1", "Content1","Footer1", export);
            StringBuilder sb = sw.GetStringBuilder();
            sb.Remove(0, sb.Length);
            report.DisplayReport();
            //Assert
            Assert.Equal("Title :Title1\nHeader :\nContent :Content1\nFooter :Footer1\n", sw.ToString());
            StreamWriter standardOutput = new StreamWriter(Console.OpenStandardOutput());
            standardOutput.AutoFlush = true;
            Console.SetOut(standardOutput);
        }

        [Fact]
        public void TestIfReportCanBeCreatedWithoutAFooter()
        {
            StringWriter sw = new StringWriter();
            Console.SetOut(sw);
            //Arrange
            Sprint sprint = new ReleaseSprint(1, "Test", new DateTime(), new DateTime());
            Report report;
            Director director = new Director();
            ReportBuilder reportBuilder = new ReportBuilder();
            Export export = new ExportPDF();
            //Act
            sprint.Director = director;
            director.SetBuilder(reportBuilder);
            report = director.ConstructReportNoFooterHeader("Title1", "Content1", "Header1", export);
            StringBuilder sb = sw.GetStringBuilder();
            sb.Remove(0, sb.Length);
            report.DisplayReport();
            //Assert
            Assert.Equal("Title :Title1\nHeader :Header1\nContent :Content1\nFooter :\n", sw.ToString());
            StreamWriter standardOutput = new StreamWriter(Console.OpenStandardOutput());
            standardOutput.AutoFlush = true;
            Console.SetOut(standardOutput);
        }
        [Fact]
        public void TestIfReportCanBeCreatedWithAHeaderAndAFooter()
        {
            StringWriter sw = new StringWriter();
            Console.SetOut(sw);
            //Arrange
            Sprint sprint = new ReleaseSprint(1, "Test", new DateTime(), new DateTime());
            Report report;
            Director director = new Director();
            ReportBuilder reportBuilder = new ReportBuilder();
            Export export = new ExportPDF();
            //Act
            sprint.Director = director;
            director.SetBuilder(reportBuilder);
            report = director.ConstructReportFooterHeader("Title1", "Content1", "Header1", "Footer1", export);
            StringBuilder sb = sw.GetStringBuilder();
            sb.Remove(0, sb.Length);
            report.DisplayReport();
            //Assert
            Assert.Equal("Title :Title1\nHeader :Header1\nContent :Content1\nFooter :Footer1\n", sw.ToString());
            StreamWriter standardOutput = new StreamWriter(Console.OpenStandardOutput());
            standardOutput.AutoFlush = true;
            Console.SetOut(standardOutput);
        }
        [Fact]
        public void TestIfReportWithoutAHeaderAndAFooterCanBeExportedToPDF()
        {
            StringWriter sw = new StringWriter();
            Console.SetOut(sw);
            //Arrange
            Sprint sprint = new ReleaseSprint(1, "Test", new DateTime(), new DateTime());
            Report report;
            Director director = new Director();
            ReportBuilder reportBuilder = new ReportBuilder();
            Export export = new ExportPDF();
            //Act
            sprint.Director = director;
            director.SetBuilder(reportBuilder);
            report = director.ConstructReportNoFooterNoHeader("Title1", "Content1", export);
            
            StringBuilder sb = sw.GetStringBuilder();
            sb.Remove(0, sb.Length);
            report.ExportReport();
            //Assert
            Assert.Equal("Export report to PDF\n", sw.ToString());
            StreamWriter standardOutput = new StreamWriter(Console.OpenStandardOutput());
            standardOutput.AutoFlush = true;
            Console.SetOut(standardOutput);
        }
        [Fact]
        public void TestIfReportWithoutAHeaderAndAFooterCanBeExportedToPNG()
        {
            StringWriter sw = new StringWriter();
            Console.SetOut(sw);
            //Arrange
            Sprint sprint = new ReleaseSprint(1, "Test", new DateTime(), new DateTime());
            Report report;
            Director director = new Director();
            ReportBuilder reportBuilder = new ReportBuilder();
            Export export = new ExportPNG();
            //Act
            sprint.Director = director;
            director.SetBuilder(reportBuilder);
            report = director.ConstructReportNoFooterNoHeader("Title1", "Content1", export);

            StringBuilder sb = sw.GetStringBuilder();
            sb.Remove(0, sb.Length);
            report.ExportReport();
            //Assert
            Assert.Equal("Export report to PNG\n", sw.ToString());
            StreamWriter standardOutput = new StreamWriter(Console.OpenStandardOutput());
            standardOutput.AutoFlush = true;
            Console.SetOut(standardOutput);
        }
        [Fact]
        public void TestIfReportWithoutAHeaderCanBeExportedToPDF()
        {
            StringWriter sw = new StringWriter();
            Console.SetOut(sw);
            //Arrange
            Sprint sprint = new ReleaseSprint(1, "Test", new DateTime(), new DateTime());
            Report report;
            Director director = new Director();
            ReportBuilder reportBuilder = new ReportBuilder();
            Export export = new ExportPDF();
            //Act
            sprint.Director = director;
            director.SetBuilder(reportBuilder);
            report = director.ConstructReportFooterNoHeader("Title1", "Content1", "Footer1", export);

            StringBuilder sb = sw.GetStringBuilder();
            sb.Remove(0, sb.Length);
            report.ExportReport();
            //Assert
            Assert.Equal("Export report to PDF\n", sw.ToString());
            StreamWriter standardOutput = new StreamWriter(Console.OpenStandardOutput());
            standardOutput.AutoFlush = true;
            Console.SetOut(standardOutput);
        }
        [Fact]
        public void TestIfReportWithoutAHeaderCanBeExportedToPNG()
        {
            StringWriter sw = new StringWriter();
            Console.SetOut(sw);
            //Arrange
            Sprint sprint = new ReleaseSprint(1, "Test", new DateTime(), new DateTime());
            Report report;
            Director director = new Director();
            ReportBuilder reportBuilder = new ReportBuilder();
            Export export = new ExportPNG();
            //Act
            sprint.Director = director;
            director.SetBuilder(reportBuilder);
            report = director.ConstructReportFooterNoHeader("Title1", "Content1","Footer1", export);

            StringBuilder sb = sw.GetStringBuilder();
            sb.Remove(0, sb.Length);
            report.ExportReport();
            //Assert
            Assert.Equal("Export report to PNG\n", sw.ToString());
            StreamWriter standardOutput = new StreamWriter(Console.OpenStandardOutput());
            standardOutput.AutoFlush = true;
            Console.SetOut(standardOutput);
        }
        [Fact]
        public void TestIfReportWithoutAFooterCanBeExportedToPDF()
        {
            StringWriter sw = new StringWriter();
            Console.SetOut(sw);
            //Arrange
            Sprint sprint = new ReleaseSprint(1, "Test", new DateTime(), new DateTime());
            Report report;
            Director director = new Director();
            ReportBuilder reportBuilder = new ReportBuilder();
            Export export = new ExportPDF();
            //Act
            sprint.Director = director;
            director.SetBuilder(reportBuilder);
            report = director.ConstructReportNoFooterHeader("Title1", "Content1", "Header1", export);

            StringBuilder sb = sw.GetStringBuilder();
            sb.Remove(0, sb.Length);
            report.ExportReport();
            //Assert
            Assert.Equal("Export report to PDF\n", sw.ToString());
            StreamWriter standardOutput = new StreamWriter(Console.OpenStandardOutput());
            standardOutput.AutoFlush = true;
            Console.SetOut(standardOutput);
        }
        [Fact]
        public void TestIfReportWithoutAFooterCanBeExportedToPNG()
        {
            StringWriter sw = new StringWriter();
            Console.SetOut(sw);
            //Arrange
            Sprint sprint = new ReleaseSprint(1, "Test", new DateTime(), new DateTime());
            Report report;
            Director director = new Director();
            ReportBuilder reportBuilder = new ReportBuilder();
            Export export = new ExportPNG();
            //Act
            sprint.Director = director;
            director.SetBuilder(reportBuilder);
            report = director.ConstructReportNoFooterHeader("Title1", "Content1", "Header1", export);

            StringBuilder sb = sw.GetStringBuilder();
            sb.Remove(0, sb.Length);
            report.ExportReport();
            //Assert
            Assert.Equal("Export report to PNG\n", sw.ToString());
            StreamWriter standardOutput = new StreamWriter(Console.OpenStandardOutput());
            standardOutput.AutoFlush = true;
            Console.SetOut(standardOutput);
        }
        [Fact]
        public void TestIfReportWithAHeaderAndAFooterCanBeExportedToPDF()
        {
            StringWriter sw = new StringWriter();
            Console.SetOut(sw);
            //Arrange
            Sprint sprint = new ReleaseSprint(1, "Test", new DateTime(), new DateTime());
            Report report;
            Director director = new Director();
            ReportBuilder reportBuilder = new ReportBuilder();
            Export export = new ExportPDF();
            //Act
            sprint.Director = director;
            director.SetBuilder(reportBuilder);
            report = director.ConstructReportFooterHeader("Title1", "Content1", "Header1", "Footer1", export);

            StringBuilder sb = sw.GetStringBuilder();
            sb.Remove(0, sb.Length);
            report.ExportReport();
            //Assert
            Assert.Equal("Export report to PDF\n", sw.ToString());
            StreamWriter standardOutput = new StreamWriter(Console.OpenStandardOutput());
            standardOutput.AutoFlush = true;
            Console.SetOut(standardOutput);
        }
        [Fact]
        public void TestIfReportWithAHeaderAndAFooterCanBeExportedToPNG()
        {
            StringWriter sw = new StringWriter();
            Console.SetOut(sw);
            //Arrange
            Sprint sprint = new ReleaseSprint(1, "Test", new DateTime(), new DateTime());
            Report report;
            Director director = new Director();
            ReportBuilder reportBuilder = new ReportBuilder();
            Export export = new ExportPNG();
            //Act
            sprint.Director = director;
            director.SetBuilder(reportBuilder);
            report = director.ConstructReportFooterHeader("Title1", "Content1", "Header1", "Footer1", export);

            StringBuilder sb = sw.GetStringBuilder();
            sb.Remove(0, sb.Length);
            report.ExportReport();
            //Assert
            Assert.Equal("Export report to PNG\n", sw.ToString());
            StreamWriter standardOutput = new StreamWriter(Console.OpenStandardOutput());
            standardOutput.AutoFlush = true;
            Console.SetOut(standardOutput);
        }
    }
}
