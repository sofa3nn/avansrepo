﻿using Domain.Persons;
using Domain.ActivityStates;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain
{
    public class Activity
    {
        public int Id { get; }

        public string Name { get; set; }

        public DateTime CreationDate { get; set; }

        public Developer? Developer { get; set; }

        //State Pattern
        public ActivityStatus Status { get; set; }  

        public Activity(int id, string name)
        {
            Id = id;
            Name = name;
            CreationDate = DateTime.Now;
            Status = new ToDo(this);
        }

        public void setStatus(ActivityStatus status)
        {
            Status = status;
        }

        public void pickUpActivity(Developer developer)
        {
            Developer = developer;
        }
    }
}
