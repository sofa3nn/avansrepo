﻿using Domain.Persons;
using Domain.Sprints.ReleaseSprints;
using Domain.Sprints.ReleaseSprints.ReleaseSprintStates;
using Domain.Sprints.ReviewSprints;
using Domain.Sprints.ReviewSprints.ReviewSprintStates;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain
{
    public class Project
    {
        public string Name { get; set; }

        public string Description { get; set; }

        public List<Sprint> Sprints { get; set; }

        public Backlog Backlog { get; set; }

        public List<Developer> Developers { get; set; }

        public ScrumMaster ScrumMaster { get; set; }

        public ProductOwner ProductOwner { get; set; }

        public Project(string name, string description, List<Developer> developers, ScrumMaster scrumMaster, ProductOwner productOwner)
        {
            Name = name;
            Description = description;
            Sprints = new List<Sprint>();
            Backlog = new Backlog();
            Developers = developers;
            ScrumMaster = scrumMaster;
            ProductOwner = productOwner;

        }
        public void AddSprint(Sprint sprint) 
        {
            Sprints.Add(sprint);
        }
        public void AddDeveloper(Developer developer) 
        { 
            Developers.Add(developer);
        }
        public void AddItemFromBacklogToSprint(Item item, Sprint sprint) 
        {
            ReleaseSprintStatus? ReleaseStatus = sprint.GetStatus() as ReleaseSprintStatus;
            ReviewSprintStatus? ReviewStatus = sprint.GetStatus() as ReviewSprintStatus;
            
            if (sprint.GetType() == new ReviewSprint(1,"",new DateTime(),new DateTime()).GetType())
            {
                if (Backlog.ItemList.Contains(item) &&
                Sprints.Contains(sprint) &&
                ReviewStatus.GetType() == new Sprints.ReviewSprints.ReviewSprintStates.Created((ReviewSprint)sprint).GetType())
                {
                    sprint.addItem(item);
                    Backlog.ItemList.Remove(item);
                }
            }
            else
            if (Backlog.ItemList.Contains(item) &&
                Sprints.Contains(sprint) &&
                ReleaseStatus.GetType() == new Sprints.ReleaseSprints.ReleaseSprintStates.Created((ReleaseSprint)sprint).GetType())
            {
                sprint.addItem(item);
                Backlog.ItemList.Remove(item);
            }
            else 
            {
                Console.WriteLine("Sprint is not part of Project or Item is not in Backlog");
            }
        }
    }
}
