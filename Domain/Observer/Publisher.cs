﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Observer
{
    public interface Publisher
    {
        void subscribe(Subscriber subscriber);
        void unsubscribe(Subscriber subscriber);
        void publish();
    }
}
