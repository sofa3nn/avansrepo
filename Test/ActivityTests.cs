using Xunit;
using Domain;
using Domain.ActivityStates;
using Domain.Persons;

namespace Test
{
    public class ActivityTests
    {
        [Fact]
        public void TestIfActivityIsCreatedWithStatusToDo()
        {
            //Arrange
            Activity activity = new Activity(0, "Tests schrijven voor activities");
            ToDo toDo = new ToDo(activity);
            //Act
            
            
            //Assert
            Assert.IsType(toDo.GetType(), activity.Status);
        }

        [Fact]
        public void TestIfActivityIsDoing()
        {
            //Arrange
            Activity activity = new Activity(0, "Tests schrijven voor activities");
            Doing Doing = new Doing(activity);

            //Act
            
            activity.setStatus(new Doing(activity));
            
            //Assert
            Assert.IsType(Doing.GetType(), activity.Status);
        }

        [Fact]
        public void TestIfActivityIsDone()
        {
            //Arrange
            Activity activity = new Activity(0, "Tests schrijven voor activities");
            Done Done = new Done(activity);

            //Act

            activity.setStatus(new Done(activity));

            //Assert
            Assert.IsType(Done.GetType(), activity.Status);
        }

        [Fact]
        public void TestIfActivityBecomesDoingWhenPickedUpFromToDo()
        {
            //Arrange
            Activity activity = new Activity(0, "Tests schrijven voor activities");
            Doing Doing = new Doing(activity);
            //Act
            activity.Status.PickUp();

            //Assert
            Assert.IsType(Doing.GetType(), activity.Status);
        }

        [Fact]
        public void TestIfActivityStaysToDoWhenGiveUpFromToDo()
        {
            //Arrange
            Activity activity = new Activity(0, "Tests schrijven voor activities");
            ToDo Status = new ToDo(activity);
            //Act
            activity.Status.GiveUp();

            //Assert
            Assert.IsType(Status.GetType(), activity.Status);

        }

        [Fact]
        public void TestIfActivityStaysToDoWhenFinishedFromToDo()
        {
            //Arrange
            Activity activity = new Activity(0, "Tests schrijven voor activities");
            ToDo Status = new ToDo(activity);
            //Act
            activity.Status.Finished();

            //Assert
            Assert.IsType(Status.GetType(), activity.Status);

        }

        [Fact]
        public void TestIfActivityBecomesToDoWhenGiveUpFromDoing()
        {
            //Arrange
            Activity activity = new Activity(0, "Tests schrijven voor activities");
            ToDo Status = new ToDo(activity);
            //Act
            activity.Status.PickUp();
            activity.Status.GiveUp();

            //Assert
            Assert.IsType(Status.GetType(), activity.Status);
        }

        [Fact]
        public void TestIfActivityStaysToDoWhenPickedUpFromDoing()
        {
            //Arrange
            Activity activity = new Activity(0, "Tests schrijven voor activities");
            Doing Status = new Doing(activity);
            //Act
            activity.Status.PickUp();
            activity.Status.PickUp();

            //Assert
            Assert.IsType(Status.GetType(), activity.Status);
        }

        [Fact]
        public void TestIfActivityBecomesDoneWhenFinishedFromDoing()
        {
            //Arrange
            Activity activity = new Activity(0, "Tests schrijven voor activities");
            Done Status = new Done(activity);
            //Act
            activity.Status.PickUp();
            activity.Status.Finished();

            //Assert
            Assert.IsType(Status.GetType(), activity.Status);
        }

        [Fact]
        public void TestIfActivityStaysDoneWhenGiveUpFromDone()
        {
            //Arrange
            Activity activity = new Activity(0, "Tests schrijven voor activities");
            Done Status = new Done(activity);
            //Act
            activity.Status.PickUp();
            activity.Status.Finished();
            activity.Status.GiveUp();

            //Assert
            Assert.IsType(Status.GetType(), activity.Status);
        }

        [Fact]
        public void TestIfActivityStaysDoneWhenPickedUpFromDone()
        {
            //Arrange
            Activity activity = new Activity(0, "Tests schrijven voor activities");
            Done Status = new Done(activity);
            //Act
            activity.Status.PickUp();
            activity.Status.Finished();
            activity.Status.PickUp();

            //Assert
            Assert.IsType(Status.GetType(), activity.Status);
        }

        [Fact]
        public void TestIfActivityStaysDoneWhenFinishedFromDone()
        {
            //Arrange
            Activity activity = new Activity(0, "Tests schrijven voor activities");
            Done Status = new Done(activity);
            //Act
            activity.Status.PickUp();
            activity.Status.Finished();
            activity.Status.Finished();

            //Assert
            Assert.IsType(Status.GetType(), activity.Status);
        }
        [Fact]
        public void TestIfActivityCanBePickedUpByOneDeveloper() 
        {
            //Arrange
            Activity activity = new Activity(0, "Test Activity");
            Developer developer = new Developer(1, "Name", "Email", "TelephoneNumber");
            //Act
            activity.pickUpActivity(developer);
            //Assert
            Assert.Equal(developer, activity.Developer);
        }
        [Fact]
        public void TestIfActivityCanExistWithoutDeveloper()
        {
            //Arrange
            Activity activity = new Activity(0, "Test Activity");
            
            //Act

            //Assert
            Assert.Null(activity.Developer);

        }
    }
}