﻿using Domain.Observer;
using Domain.Sprints.ReleaseSprints.ReleaseSprintStates;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Sprints.ReleaseSprints
{
    public class ReleaseSprint : Sprint, Publisher
    {
        public ReleaseSprintStatus Status { get; set; }
        public List<Subscriber> Subscribers = new List<Subscriber>();

        public ReleaseSprint(int id, string name, DateTime start, DateTime end) : base(id, name, start, end)
        {
            Status = new Created(this);
        }

        public void setStatus(ReleaseSprintStatus status)
        {
            Status = status;
            publish();
        }

       

        public void subscribe(Subscriber subscriber)
        {
            Subscribers.Add(subscriber);
        }

        public void unsubscribe(Subscriber subscriber)
        {
            Subscribers.Remove(subscriber);
        }

        public void publish()
        {
            foreach (Subscriber subscriber in Subscribers)
            {
                subscriber.updateReleaseSprint(Status);
            }
        }

        public override object GetStatus()
        {
            return Status;
        }
    }

    
}
