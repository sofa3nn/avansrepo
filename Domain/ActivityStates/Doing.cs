﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.ActivityStates
{
    public class Doing : ActivityStatus
    {
        public Doing(Activity activity) : base(activity)
        {
        }

        public override void GiveUp()
        {
            Activity.setStatus(new ToDo(Activity));
            Console.WriteLine("This Activity is returned to the list.");
        }

        public override void PickUp()
        {
            Console.WriteLine("This Activity is already picked up.");
        }
        
        public override void Finished()
        {
            Activity.setStatus(new Done(Activity));
            Console.WriteLine("This Activity is done.");
        }
    }
}
