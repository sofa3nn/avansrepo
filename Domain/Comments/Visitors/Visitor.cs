﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Comments.Visitors
{
    public abstract class Visitor
    {
        public abstract void VisitMessageGroup(MessageGroup messageGroup);
        public abstract void VisitComment(Comment comment, int depth);
    }
}
