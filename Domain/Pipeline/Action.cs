﻿using Domain.Composites;
using Domain.Pipeline.Visitors;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Pipeline
{
    public class Action : Component
    {
        public string Command { get; set; }

        public Action(string command)
        {
            Command = command;
        }

        public override void AcceptVisitor(Object obj, int depth)
        {
            Visitor? visitor = obj as Visitor;
            visitor?.VisitAction(this, depth);
        }
    }
}
