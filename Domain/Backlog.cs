﻿namespace Domain
{
    public class Backlog
    {
        public List<Item> ItemList { get; private set; }

        public Backlog()
        {
            ItemList = new List<Item>();
        }

        public void addItem(Item item)
        {
            ItemList.Add(item);
        }
    }
}