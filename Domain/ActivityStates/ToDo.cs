﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.ActivityStates
{
    public class ToDo : ActivityStatus
    {
        
        public ToDo(Activity activity) : base(activity)
        {
        }

        public override void GiveUp()
        {
            Console.WriteLine("This Activity is not picked up yet.");
        }

        public override void PickUp()
        {
            Activity.setStatus(new Doing(Activity));
            Console.WriteLine("This Activity is picked up.");
        }

        public override void Finished()
        {
            Console.WriteLine("This Activity is not picked up yet, you can't finish it without picking it up first.");
        }

    }
}
